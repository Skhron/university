create table if not exists treatment
(
	purpose_code text not null
		constraint treatment_pkey
			primary key,
	disease text not null
)
;

alter table treatment owner to postgres
;

create table if not exists purpose
(
	purpose_code_id text not null
		constraint purpose_pkey
			primary key
		constraint purpose_purpose_code_id_fkey
			references treatment,
	"group" text not null,
	description text not null
)
;

alter table purpose owner to postgres
;

create table if not exists form_of_issue
(
	id integer not null
		constraint form_of_issue_pkey
			primary key,
	form text not null
)
;

alter table form_of_issue owner to postgres
;

create unique index if not exists form_of_issue_form
	on form_of_issue (form)
;

create table if not exists providers
(
	provider_code integer not null
		constraint providers_pkey
			primary key,
	name text not null,
	representative text not null,
	position text not null,
	address text,
	city text not null,
	country text not null,
	telephone text not null
)
;

alter table providers owner to postgres
;

create table if not exists medicament
(
	medicament_code integer not null
		constraint medicament_pkey
			primary key,
	medicament_name text not null,
	purpose_code_id text not null
		constraint medicament_purpose_code_id_fkey
			references purpose,
	provider_code_id integer not null
		constraint medicament_provider_code_id_fkey
			references providers,
	count integer not null,
	unit text not null,
	form_of_issue_id integer not null
		constraint medicament_form_of_issue_id_fkey
			references form_of_issue,
	price real not null
)
;

alter table medicament owner to postgres
;

create index if not exists medicament_purpose_code_id
	on medicament (purpose_code_id)
;

create index if not exists medicament_provider_code_id
	on medicament (provider_code_id)
;

create index if not exists medicament_form_of_issue_id
	on medicament (form_of_issue_id)
;

create table if not exists users
(
	id serial not null
		constraint users_pkey
			primary key,
	user_login text not null
)
;

alter table users owner to postgres
;

create table if not exists cart
(
	id serial not null
		constraint cart_pkey
			primary key,
	cart_id integer not null,
	cart_product_code_id integer not null
		constraint cart_cart_product_code_id_fkey
			references medicament,
	cart_price real not null,
	cart_count integer not null,
	cart_user_id integer not null
		constraint cart_cart_user_id_fkey
			references users
)
;

alter table cart owner to postgres
;

create index if not exists cart_cart_product_code_id
	on cart (cart_product_code_id)
;

create index if not exists cart_cart_user_id
	on cart (cart_user_id)
;