import asyncio
import logging
import mailbox

from aiosmtpd.controller import Controller
from aiosmtpd.handlers import Message


class MyMailbox(Message):
    def __init__(self, message_class=None):
        super().__init__(message_class)

    def handle_message(self, message):
        mail_dir = 'MailFolder/' + message['To']
        self.mailbox = mailbox.Maildir(mail_dir)
        self.mailbox.add(message)

    def reset(self):
        self.mailbox.clear()

    @classmethod
    def from_cli(cls, parser, *args):
        if len(args) < 1:
            parser.error('The directory for the maildir is required')
        elif len(args) > 1:
            parser.error('Too many arguments for Mailbox handler')
        return cls(args[0])



async def amain(loop):
    box = MyMailbox()
    cont = Controller(box, hostname='::0', port=8025)
    cont.start()


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    loop = asyncio.get_event_loop()
    loop.create_task(amain(loop=loop))
    try:
        loop.run_forever()
    except KeyboardInterrupt:
        pass












