from flask import render_template, request, jsonify
import db.db_spec as db_spec, db.db_search as db_search, db.db_users as db_users, db.db_cart as db_cart
import Smtp_server.sending_mail as smtp

class button_handlers():
    def del_from_cart(self):
        user = db_users.User()
        cart = db_cart.Carts()

        login = request.args.get('login')
        med_id = request.args.get('med_id')

        user_id = user.is_registred(login)
        if user_id != -1:
            cart.delete_from_cart(user_id, int(med_id))
            return jsonify("Done!")
        else:
            return jsonify("Faild!")

    def add_to_cart(self):
        user = db_users.User()
        cart = db_cart.Carts()

        login = request.args.get('login')
        med_id = request.args.get('med_id')

        user_id = user.is_registred(login)
        if user_id != -1:
            if (cart.add_to_cart(user_id, med_id, 1) == -1):
                return jsonify(1)
            else:
                return jsonify(0)
        else:
            return jsonify(1)

    def buy_on_cart(self):
        user = db_users.User()
        cart = db_cart.Carts()

        login = request.args.get('login')
        user_id = user.is_registred(login)
        mail_reciv = str(login) + "@example.com"
        if user_id != -1:
            msg = cart.buy_cart(user_id)
            smtp.send_mail("recipient@example.com", mail_reciv, "thx for purchases", "files.txt")
            return jsonify(0)
        else:
            return jsonify(1)

    def get_category_page(self):
        spec = db_spec.Spec()
        search = db_search.Search()

        category = request.args.get('category')

        data = spec.list_to_dict(search.search_by_categoty(category))
        data2 = spec.list_to_dict(spec.get_categories())

        return render_template("main.html", data=data, data2=data2)

    def do_search(self):
        spec = db_spec.Spec()
        search = db_search.Search()

        name = request.args.get('name')
        provider_name = request.args.get('provider_name')
        purpose_code = request.args.get('purpose_code')
        min_price = request.args.get('min_price')
        max_price = request.args.get('max_price')

        try:
            min_price_float = float(min_price)
        except Exception:
            min_price_float = 0

        try:
            max_price_float = float(max_price)
        except Exception:
            max_price_float = 10000

        if name == None:
            name = ""
        if provider_name == None:
            provider_name = ""
        if purpose_code == None:
            purpose_code = ""

        search_params = dict()
        search_params["name"] = str(name)
        search_params["provider_name"] = str(provider_name)
        search_params["purpose_code"] = str(purpose_code)
        search_params["min_price"] = min_price
        search_params["max_price"] = max_price

        data = spec.med_spec_dict(
            search.search_composite(name, min_price_float, max_price_float, provider_name, purpose_code))
        return render_template("search.html", data=data, sp=search_params)

