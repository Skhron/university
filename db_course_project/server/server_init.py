from flask import Flask

from Views import MainView, CartView, SearchView, ProductView

import button_handlers


app = Flask(__name__, static_folder='static')

handlers = button_handlers.button_handlers()

app.add_url_rule('/', '', view_func=MainView.as_view('main'))
app.add_url_rule('/do_search', 'do_search', handlers.do_search)
app.add_url_rule('/get_category_page', 'get_category_page', handlers.get_category_page)
app.add_url_rule('/buy_on_cart', 'buy_on_cart', handlers.buy_on_cart)
app.add_url_rule('/add_to_cart', 'add_to_cart', handlers.add_to_cart)
app.add_url_rule('/del_from_cart', 'del_from_cart', handlers.del_from_cart)
app.add_url_rule('/main', view_func=MainView.as_view('main'))
app.add_url_rule('/search', view_func=SearchView.as_view('search'))
app.add_url_rule('/cart', view_func=CartView.as_view('cart'))
app.add_url_rule('/product', view_func=ProductView.as_view('product'))

if __name__ == "__main__":
    app.run()
