from db.db_models import *

class Search:
    def search_with_name(self, name):
        result = list(Medicament.select().where(Medicament.medicament_name.contains(name)))

        return result

    def search_with_code(self, code):
        result = list(Medicament.select().where(Medicament.medicament_code == code))

        return result

    def search_by_categoty(self, category):
        result = list(Medicament.select()
                .where(Medicament.purpose_code.in_(Purpose.select().where(Purpose.group == category))))

        return result


    def search_by_provider_name(self, name):
        result = list(Medicament.select().join(Providers).where(Providers.name.contains(name)))

        return result


    def search_composite(self, name="", price_from=0, price_to=10000, provider_name="", purpose_code=""):
        q = Medicament.select().join(Providers).where(Providers.name.contains(provider_name))
        q1 = q.select().where(
            (Medicament.price >= price_from) & (Medicament.price <= price_to) & (Medicament.medicament_name.contains(name)))

        result = list(q1.select().where(Medicament.purpose_code.contains(purpose_code)))

        return result

    def sort_by_field(self, query, condition):
        result = query.order_by(condition)

        return result

    def take_n_rows(self, n):
        result = list(Medicament.select().limit(n))

        return result