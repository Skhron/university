from db.db_models import *

class Spec:
    def print_query_result(self, q):
        for i in q:
            print(i)


    def get_categories(self):
        return list(Purpose.select())


    def list_to_dict(self, list_data):
        dict_list_data = list()
        for i in list_data:
            #print("This is I: ", i.__dict__)
            dict_list_data.append(i.__dict__["__data__"])

        return dict_list_data

    def med_spec_dict(self, list_f):
        j = 0
        dict_list_data = list()
        dict_list_data = self.list_to_dict(list_f)

        for i in list_f:
            dict_list_data[j]["form_of_issue"] = i.form_of_issue.form
            dict_list_data[j]["provider_code"] = i.provider_code.name
            j = j+1

        return dict_list_data

    def product_spec_dict(self, list_f):
        j = 0
        dict_list_data = list()
        dict_list_data = self.list_to_dict(list_f)

        for i in list_f:
            dict_list_data[j]["purpose_code"] = i.purpose_code.description
            dict_list_data[j]["disease"] = i.purpose_code.purpose_code.disease
            dict_list_data[j]["provider_code"] = i.provider_code.name
            dict_list_data[j]["form_of_issue"] = i.form_of_issue.form
            j = j+1

        return dict_list_data

    def cart_spec_dict(self, list_f, all_price):
        j = 0
        dict_list_data = list()
        dict_list_data = self.list_to_dict(list_f)

        for i in list_f:
            dict_list_data[j]["cart_product_name"] = i.cart_product_code.medicament_name
            dict_list_data[j]["all_price"] = all_price
            j = j+1

        return dict_list_data