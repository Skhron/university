from peewee import *

db = PostgresqlDatabasedb = PostgresqlDatabase(
    'pharmacy_database',  # Required by Peewee.
    user='postgres',  # Will be passed directly to psycopg2.
    password='tr2324241',  # Ditto.

)

class Users(Model):
    user_login = TextField(null=False)

    class Meta:
        database = db


class Providers(Model):
    provider_code = IntegerField(primary_key=True, null=False, unique=True)
    name = TextField(null=False)
    representative = TextField(null=False)
    position = TextField(null=False)
    address = TextField(255)
    city = TextField(null=False)
    country = TextField(null=False)
    telephone = TextField(null=False)

    class Meta:
        database = db


class Treatment(Model):
    purpose_code = TextField(primary_key=True, unique=True)
    disease = TextField(null=False)

    class Meta:
        database = db


class Purpose(Model):
    purpose_code = ForeignKeyField(Treatment, primary_key=True, unique=True)
    group = TextField(null=False)  #, unique=True)
    description = TextField(null=False)

    class Meta:
        database = db


class Form_of_issue(Model):
    id = IntegerField(primary_key=True, null=False, unique=True)
    form = TextField(null=False, unique=True)

    class Meta:
        database = db


class Medicament(Model):
    medicament_code = IntegerField(primary_key=True, unique=True)
    medicament_name = TextField(null=False)
    purpose_code = ForeignKeyField(Purpose)
    provider_code = ForeignKeyField(Providers, related_name='prov_code')
    count = IntegerField(null=False)
    unit = TextField(null=False)
    form_of_issue = ForeignKeyField(Form_of_issue)
    price = FloatField(null=False)

    class Meta:
        database = db


class Cart(Model):
    # id = IntegerField(primary_key=True, unique=True)
    cart_id = IntegerField(null=False)
    cart_product_code = ForeignKeyField(Medicament)
    cart_price = FloatField(null=False)
    cart_count = IntegerField(null=False)
    cart_user = ForeignKeyField(Users)

    class Meta:
        database = db
