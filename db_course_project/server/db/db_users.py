from db.db_models import *
import random

class User:
    def is_registred(self, login):
        try:
            q = Users.get(Users.user_login == login).id
        except Users.DoesNotExist:
            q = -1

        return q

    def add_user(self):

        login = str(random.randint(100000, 999999))
        while self.is_registred(login) != -1:
            login = str(random.randint(100000, 999999))

        q = Users.create(user_login=login)

        return login
