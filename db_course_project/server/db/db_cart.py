from db.db_models import *

class Carts:
    def get_cart(self, user_id):
        all_price = 0
        result = list(Cart.select().where(Cart.cart_user == user_id))
        for i in result:
            all_price += i.cart_price * i.cart_count

        return result, all_price

    def buy_cart(self, user_id):
        result = list(Cart.select().where(Cart.cart_user == user_id))
        msg = ""
        for i in result:
            if (Medicament.medicament_code == i.cart_product_code):
                med = Medicament.get(Medicament.medicament_code == i.cart_product_code)
                msg += med.medicament_name + "\n"
                msg += str(med.price)
            q = Medicament.update(count=Medicament.count - i.cart_count).where(Medicament.medicament_code == i.cart_product_code)
            q.execute()
            q1 = Cart.get((Cart.cart_user == user_id) & (Cart.cart_product_code == i.cart_product_code))
            q1.delete_instance()
        return msg


    def add_to_cart(self, user_id, medicament_id, count):
        med = Medicament.get(Medicament.medicament_code == medicament_id)

        if (med.count < count):
            return -1

        try:
            q = Cart.get((Cart.cart_product_code == medicament_id) & (Cart.cart_user == user_id))
            ex = Cart.update(cart_count=count + q.cart_count).where(Cart.id == q.id)
            ex.execute()

        except Cart.DoesNotExist:
            cart_id = user_id * 13
            c = Cart(cart_id=cart_id, cart_product_code=medicament_id, cart_price=med.price * count, cart_count=count,
                 cart_user_id=user_id)
            c.save()

        return 0


    def delete_from_cart(self, user_id, med_id):
        q = Cart.get((Cart.cart_user == user_id) & (Cart.cart_product_code == med_id))
        q.delete_instance()

        return 0