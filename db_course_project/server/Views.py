from flask import request, render_template, make_response
from flask.views import View

import db.db_users as db_users, db.db_spec as db_spec, db.db_search as db_search, db.db_cart as db_cart

class MainView(View):
    def dispatch_request(self):
        spec = db_spec.Spec()
        user = db_users.User()

        login = request.cookies.get('login')
        print("init = ", str(login))

        search = db_search.Search()

        data = spec.med_spec_dict(search.take_n_rows(14))
        data2 = spec.list_to_dict(spec.get_categories())

        if login is None or user.is_registred(login) == -1:
            login = user.add_user()
            resp = make_response(render_template("main.html", data=data, data2=data2))
            resp.set_cookie('login', value=login)
            return resp
        else:

            return render_template("main.html", data=data, data2=data2)

class SearchView(View):
    def dispatch_request(self):
        search_params = dict()
        search_params["name"] = ""
        search_params["provider_name"] = ""
        search_params["purpose_code"] = ""
        search_params["min_price"] = ""
        search_params["max_price"] = ""

        data = list()

        return render_template("search.html", data=data, sp=search_params)

class CartView(View):
    def dispatch_request(self):
        spec = db_spec.Spec()
        user = db_users.User()
        cart = db_cart.Carts()

        login = request.cookies.get('login')
        user_id = user.is_registred(login)

        data1, all_price = cart.get_cart(user_id)
        dict_1 = spec.cart_spec_dict(data1, all_price)

        return render_template("cart.html", data=dict_1)

class ProductView(View):
    def dispatch_request(self):
        spec = db_spec.Spec()
        search = db_search.Search()

        code = request.args.get('code')

        data = spec.product_spec_dict(search.search_with_code(int(code)))
        return render_template("product.html", data=data)

