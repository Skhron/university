$(document).ready(function() {
    console.log( "ready!" );
    // Обработчиек события нажатия на кнопку "добавить в корзину"
    $(".button_action_main").on("click", function () {
        var id = this.id;
        var login = $.cookie("login");
       $.get("/add_to_cart", {med_id: id, login: login}).done(function (data) {
           console.log( data );
           if (data != 1) {
            alert("Товар добавлен!");
           } else {
            alert("Товара нет на складе!");
           }
        });
    });

    // Обработчик нажатия на кнопку "Удалить" на странице корзины
    $(".button_action_delete").on("click", function () {
        console.log( "on air" );
        var id = this.id;
        var login = $.cookie("login");
       $.get("/del_from_cart", {med_id: id, login: login}).done(function (data) {
           window.location.href = "/cart";
           console.log( data );
           alert("Товар удален!")
        });

    });

    // Обработчик нажатия на кнопку "Купить" на странице корзины
    $(".button_action_buy").on("click", function () {
        console.log( "on air" );
        var id = this.id;
        var login = $.cookie("login");
       $.get("/buy_on_cart", {login: login}).done(function (data) {
           window.location.href = "/cart";
           console.log( data );
           alert("Спасибо за покупки!")
        });

    });

    // Выбор пункта в меню категорий
    $(".list-group-item-action").on("click", function () {
        var id = this.text;
        window.location.href = "/get_category_page?category=" + id
     });

    // Обрабочик события нажатия на кнопку "Поиск" на странице поиска
    $(".button_action_search").on("click", function () {
                var name = $("#name-search-input-1").val();
                var provider_name = $("#name-search-input-2").val();
                var purpose_code = $("#name-search-input-3").val();
                var min_price = $("#name-search-input-4").val();
                var max_price = $("#name-search-input-5").val();

        console.log(name);

        console.log(provider_name);
        console.log(purpose_code);
        console.log(min_price);
        console.log(max_price);
        if (parseFloat(min_price) > parseFloat(max_price))
        {
            alert("Минимальная цена не должна быть больше максимальной!");
            return 0
        }
        else {
            window.location.href = "/do_search?name=" + name + "&provider_name=" + provider_name +
                "&purpose_code=" + purpose_code + "&min_price=" + min_price + "&max_price=" + max_price;

        }

    });

});