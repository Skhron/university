FROM python:3

ADD db_course_project/server/server_init.py /db_course_project/server/

COPY requirements.txt /tmp

WORKDIR /tmp

RUN pip install -r requirements.txt