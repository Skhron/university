package graph;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

/**
 *
 * @author Sh0N
 */
public class Graph {

    static class List {
        int vNum; // ���������� ������
        int eNum; // ���������� �����
        String [] city; // ������ ������������ 
        int [][] graph1; // ������� ��������� ��������
        int [][] graph2; // ������� ��������� ���������
 
        List(String name1, String name2, String name3){
            try { 
                Scanner scanner = new Scanner(new BufferedReader(new FileReader(name1)));
                this.vNum = 0;
                int t;
                t = scanner.nextInt();
                this.city = new String[t];
                scanner.nextLine();
                while (scanner.hasNextLine())
                    this.city[this.vNum++] = scanner.nextLine();
            } 
            catch (FileNotFoundException fnfe){
                System.out.print("���� �� ������! \n");
                System.exit(1);
            }
            
            try { 
                Scanner scanner = new Scanner(new BufferedReader(new FileReader(name2)));
                int t;
                int i = 0;
                int j = 0;
                int res;
                t = scanner.nextInt();
                this.graph1 = new int[t][t]; 
                while (scanner.hasNextInt()) {
                    res = scanner.nextInt();
                    if (i != t)
                        graph1[j][i++] = res;
                    else{
                        j++;
                        i = 0;
                        graph1[j][i++] = res;
                    }              
                }
            } 
            catch (FileNotFoundException fnfe){
                System.out.print("���� �� ������! \n");
                System.exit(1);
            }
            
            try { 
                Scanner scanner = new Scanner(new BufferedReader(new FileReader(name3)));
                int t;
                int i = 0;
                int j = 0;
                int res;
                t = scanner.nextInt();
                this.graph2 = new int[t][t]; 
                while (scanner.hasNextInt()) {
                    res = scanner.nextInt();
                    if (i != t)
                        graph2[j][i++] = res;
                    else{
                        j++;
                        i = 0;
                        graph2[j][i++] = res;
                    }              
                }
            } 
            catch (FileNotFoundException fnfe){
                System.out.print("���� �� ������! \n");
                System.exit(1);
            }
        }
        
        void justDFS(int v, int graph[][], boolean used[]) {
            used[v] = true; // �������� �������
            for (int nv = 0; nv < vNum; nv++) // ���������� �������
                if (!used[nv] && (graph[v][nv] > 0)) // ���� ������� �� ��������, � ������ � �������
                    justDFS(nv, graph, used); // ���������� ��������� �� ��� DFS
        }
             
        void search(int g[][], int dist[][],int h[][]){
            //int[][] dist = new int[this.vNum][this.vNum];
            for(int i = 0; i < this.vNum; i++)
                System.arraycopy(g[i], 0, dist[i], 0, this.vNum);
            for(int i = 0; i < this.vNum; i++)
                for(int j = 0; j < this.vNum; j++)
                    if (g[i][j] > 0)
                        h[i][j] = j+1;
            for(int k = 0; k < this.vNum; k++)
                for(int i = 0; i < this.vNum; i++)
                    for(int j = 0; j < this.vNum; j++)
                        if ((((dist[i][j] > dist[i][k]+dist[k][j])) || (dist[i][j] < 0)) && (dist[i][k] > 0) && (dist[k][j] > 0)){
                            dist[i][j] = dist[i][k]+dist[k][j];
                            h[i][j] = h[i][k];
                        }
        }
        
        static void Read(String text[], String name) throws FileNotFoundException, IOException{
        System.getProperty("line.separator");
        PrintWriter writer = null;
        try {
            writer = new PrintWriter(new FileWriter(name));
            for (String text1 : text) {
                if (text1 != null) {
                    writer.write(text1);
                }
            }
            writer.flush();
        } finally {}
        } 
/*        
        void solve() {
            used = new boolean [vNum]; // ������ �������
            cc = new int [vNum]; // ��[v] = ����� ����������, � ������� ����������� v
            ccNum = 0; // ���������� ���������
         
            for (int v = 0; v < vNum; v++) { // ���������� �������
                if (!used[v]) { // ���� ������� �� ��������
                    ccNum++; // ������ �� ����� ���������� ��������� 
                    dfs(v); // ��������� �� ��� DFS
                }
            }
        }
     
        void dfs(int v) {
            used[v] = true;
            cc[v] = ccNum; // ������ ������� ������� � ������������ ����� ����������
            for (int nv = 0; nv < vNum; nv++)
                if (!used[nv] && graph[v][nv])
                    dfs(nv);
        }
    }
*/
    }
    
    public static void main(String[] args) throws IOException {
        int z = 0;
        int exc = 0;
        String s1,s2,s3,s4;
        List list = new List("a.txt","b.txt","c.txt");
        Scanner sc = new Scanner(System.in);
        do {
            System.out.print("1 - ������ ���� \n");
            System.out.print("2 - ������� ���� \n");
            System.out.print("3 - ������ � ������ \n");
            System.out.print("4 - �������� ������ \n");
            System.out.print("5 - �������� ����������� \n");
            System.out.print("6 - ���������� �������� ������ ����������� ��������� \n");
            System.out.print("7 - ����� \n");
            s1 = sc.nextLine();
            z = Integer.parseInt(s1);
            switch (z){
                case 1:
                    System.out.print("������� �������� ����� ����������� �������� �������: \n");
                    s2 = sc.nextLine();
                    System.out.print("������� �������� ����� ����������� ������� ��������� �������� ������: \n");
                    s3 = sc.nextLine();
                    System.out.print("������� �������� ����� ����������� ������� ��������� ��������� ������: \n");
                    s4 = sc.nextLine();
                    list = new List(s2,s3,s4);
                    exc = 1;
                    break;
                case 2:
                    int p = 5;
                    String[] text = new String [100+list.vNum];
                    text[0] = "digraph TG {  \n";
                    text[1] = "rankdir=LR; \n";
                    text[2] = "size=\"8,5\"";
                    text[3] = "node [shape = circle]; \n";
                    text[4] = "edge[dir=both, minlen=2]; \n";
                    for(int i = 0; i < list.vNum; i++)
                        for (int j = i; j < list.vNum; j++){
                            if (list.graph1[i][j] > 0)
                                text[p++] = list.city[i] + " -> "+ list.city[j] +"[label = \""+list.graph1[i][j]+" rail"+"\"];";
                            if (list.graph2[i][j] > 0)
                                text[p++] = list.city[i] + " -> "+ list.city[j] +"[label = \""+list.graph2[i][j]+" high"+"\"];";
                        }
                    for(int i = 0; i < list.vNum; i++){           // wip
                        int g = 0;
                        for (int j = i; j < list.vNum; j++)
                            if ((list.graph1[i][j] <= 0) && (list.graph2[i][j] <= 0))
                                g++;
                        if (g == list.vNum - i)
                            text[p++] = list.city[i]+";";
                    }
                    //text[5] = "LR_0 -> LR_1 [label = \"work\"];";
                    //biTree.prefix_export_tree(biTree,text,p);
                    text[p] = "} ";
                    list.Read(text,"out1.txt");
                    String command1 = "cmd /c start cmd.exe /c dot -Tpng -o out1.png out1.txt";
                    Process child1 = Runtime.getRuntime().exec(command1);
                    break;
                case 3:
                    int z1 = 0;
                    do {
                        int i = 0;
                        System.out.print("1 - ������� ������� \n");
                        System.out.print("2 - �������� ������� \n");
                        System.out.print("3 - ����� \n");
                        s2 = sc.nextLine();
                        z1 = Integer.parseInt(s2);
                        switch (z1){
                            case 1:
                                System.out.print("������� �������� �������: \n");
                                s2 = sc.nextLine();
                                while(i < list.vNum)
                                    if (s2 == null ? list.city[i] == null : s2.equals(list.city[i])){
                                        for (int j = i; j < list.vNum-1; j++){
                                            list.city[j] = list.city[j+1];
                                        }
                                        break;
                                    }
                                    else
                                        i++;
                                if (list.vNum == 0){
                                    System.out.print("��� ������! \n");
                                    break;
                                }
                                if (i == list.vNum){
                                    System.out.print("����� ������� �� ����������! \n");
                                    break;
                                }
                                int m = 0, n = 0;
                                int d[][] = new int[list.vNum-1][list.vNum-1];
                                int b[][] = new int[list.vNum-1][list.vNum-1];
                                for(int j = 0; j < list.vNum; j++)
                                    if (j != i){
                                        for(int k = 0; k < list.vNum; k++)
                                            if (k != i){
                                                d[m][n] = list.graph1[j][k];
                                                b[m][n] = list.graph2[j][k];
                                                n++;
                                            }
                                        m++;
                                        n = 0;
                                    }
                                list.graph1 = d.clone();
                                list.graph2 = b.clone();
                                list.vNum--;        
                                break;
                            case 2:           // ����� ������������ ��������� ��� ���...
                                int err = 0;
                                String[] a = new String[list.vNum+1];
                                int [][] tmp1 = new int[list.vNum+1][list.vNum+1];
                                int [][] tmp2 = new int[list.vNum+1][list.vNum+1];
                                System.out.print("������� �������� �������: \n");
                                s2 = sc.nextLine();
                                for(int j = 0; j < list.vNum; j++)
                                    if (s2 == null ? list.city[j] == null : s2.equals(list.city[j])){
                                        System.out.print("����� ������� ��� ����������");
                                        err = 1;
                                        break;
                                    }
                                if (err == 1)
                                    break;
                                System.arraycopy(list.city, 0, a, 0, list.vNum);
                                for (int j = 0; j < list.vNum; j++)
                                    for (int k = 0; k < list.vNum; k++){
                                        tmp1[j][k] = list.graph1[j][k];
                                        tmp2[j][k] = list.graph2[j][k];
                                    }
                                a[list.vNum] = s2;
                                System.out.print("������� ��������� �������� ������: \n");
                                for (int j = 0; j < list.vNum; j++)
                                {
                                    System.out.print(list.city[j] + ": \n");
                                    s3 = sc.nextLine();
                                    tmp1[j][list.vNum] = Integer.parseInt(s3);
                                    tmp1[list.vNum][j] = Integer.parseInt(s3);
                                }
                                tmp1[list.vNum][list.vNum] = 0;
                                System.out.print("������� ��������� ��������� ������: \n");
                                for (int j = 0; j < list.vNum; j++)
                                {
                                    System.out.print(list.city[j] + ": \n");
                                    s3 = sc.nextLine();
                                    tmp2[j][list.vNum] = Integer.parseInt(s3);
                                    tmp2[list.vNum][j] = Integer.parseInt(s3);
                                }
                                tmp2[list.vNum][list.vNum] = 0;
                                list.vNum++;
                                list.city = new String[list.vNum];
                                list.graph1 = new int[list.vNum][list.vNum];
                                list.graph2 = new int[list.vNum][list.vNum];
                                list.city = (String[])a.clone();
                                list.graph1 = (int [][])tmp1.clone();
                                list.graph2 = (int [][])tmp2.clone();
                                break;
                        }
                    }while (z1 != 3);
                    break;
                case 4:
                    int d1[][] = new int[list.vNum][list.vNum];
                    int d2[][] = new int[list.vNum][list.vNum];
                    int h1[][] = new int[list.vNum][list.vNum];
                    int h2[][] = new int[list.vNum][list.vNum];
                    list.search(list.graph1,d1,h1);
                    list.search(list.graph2,d2,h2);
                    System.out.print("������� ���������� ���������� �������� ������: \n");
                    for (int j = 0; j < list.vNum; j++){
                        for (int i = 0; i < list.vNum; i++){
                            System.out.print(d1[j][i]+" ");
                        }
                        System.out.println();
                    }
                    System.out.println();
                    
                    System.out.print("������� �������������� ���������� ���������� �������� ������: \n");
                    for (int j = 0; j < list.vNum; j++){
                        for (int i = 0; i < list.vNum; i++){
                            System.out.print(h1[j][i]+" ");
                        }
                        System.out.println();
                    }
                    System.out.println();
                    
                    System.out.print("������� ���������� ���������� ��������� ������: \n");
                    for (int j = 0; j < list.vNum; j++){
                        for (int i = 0; i < list.vNum; i++){
                            System.out.print(d2[j][i]+" ");
                        }
                        System.out.println();
                    }
                    System.out.println();
                    
                    System.out.print("������� �������������� ���������� ���������� ��������� ������: \n");
                    for (int j = 0; j < list.vNum; j++){
                        for (int i = 0; i < list.vNum; i++){
                            System.out.print(h2[j][i]+" ");
                        }
                        System.out.println();
                    }
                    System.out.println();
                    
                    break;
                case 5:
                    if (exc == 1){
                        int kek = 0;
                        boolean [] used = new boolean[list.vNum];
                        System.out.print("������� �������� �������: \n");
                        s2 = sc.nextLine();
                        for(int i = 0; i < list.vNum; i++)
                            if (s2 == null ? list.city[i] == null : s2.equals(list.city[i]))
                                kek = 1;
                        if (kek == 0){
                            System.out.print("����� ������� ���! \n");
                            break;
                        }
                        for (int i = 0; i < list.vNum; i++)
                            if (s2 == null ? list.city[i] == null : s2.equals(list.city[i])){
                                list.justDFS(i,list.graph1,used);
                                list.justDFS(i,list.graph2,used);                                   
                            }
                        System.out.print("������� "+s2+" �������� � ���������: ");
                        for (int i = 0; i < list.vNum; i++)
                            if (used[i] == true)
                                System.out.print(list.city[i]+", ");
                        System.out.println();
                    }
                    break;
                case 6:
                    int u = 5;
                    String[] text1 = new String [100+list.vNum];
                    text1[0] = "digraph TG {  \n";
                    text1[1] = "rankdir=LR; \n";
                    text1[2] = "size=\"8,5\"";
                    text1[3] = "node [shape = circle]; \n";
                    text1[4] = "edge[dir=both, minlen=2]; \n";
                    int d3[][] = new int[list.vNum][list.vNum];
                    int d4[][] = new int[list.vNum][list.vNum];
                    int h[][] = new int[list.vNum][list.vNum];
                    list.search(list.graph1,d3,h);
                    list.search(list.graph2,d4,h);
                    for(int i = 0; i < list.vNum; i++)
                        for (int j = i; j < list.vNum; j++){
                            if ((d3[i][j] > 0) && ((d3[i][j] <=  d4[i][j]) || (d4[i][j] <= 0)))
                                text1[u++] = list.city[i] + " -> "+ list.city[j] +"[label = \""+d3[i][j]+" rail"+"\"];";
                            if ((d4[i][j] > 0)  && ((d3[i][j] >  d4[i][j]) || (d3[i][j] <= 0)))
                                text1[u++] = list.city[i] + " -> "+ list.city[j] +"[label = \""+d4[i][j]+" high"+"\"];";
                        }
                    //text[5] = "LR_0 -> LR_1 [label = \"work\"];";
                    //biTree.prefix_export_tree(biTree,text,p);
                    text1[u] = "} ";
                    list.Read(text1,"out2.txt");
                    String command2 = "cmd /c start cmd.exe /c dot -Tpng -o out2.png out2.txt";
                    Process child2 = Runtime.getRuntime().exec(command2);
                    break;
            }
        }while (z != 7);
    }    
}
