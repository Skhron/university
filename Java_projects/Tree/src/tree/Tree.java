/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tree;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

/**
 *
 * @author Sh0N
 */
public class Tree {
    
    static class Deque {
        int size;              //������ �������
        int tail;              //��������� 2-�� �����
        int[] data;            //������ ������
        
        Deque(int size) {     //����������������� �������
            this.size = size;
            data = new int[size];
            this.tail = 0;
        }
        
        void PushBack(int value) {    //���������� 1-�� ����� ������ ����
            if (tail == size) {
                //System.out.println("���� ����������");
                //tail = 0;
            }
            data[tail++] = value;
        }
        void PopBack() {               //�������� ���������� ����������� �������� 1-�� �����
            if (--tail < 0)
                tail = size - 1;
            data[tail] = 0;
        }
        
        long Fun(int t[]){
            long f1,f2;
            f1 = System.nanoTime();
            for (int i = 0; i < 8; i++)
                PushBack(t[i]);
            for (int i = 0; i < 8; i++)
                PopBack();
            f2 = System.nanoTime()-f1;
            return f2;
        }
    }    
    
    static class Node{
        int key;                    // ���� ����
        Node left;                // ��������� �� ������ �������
        Node right;               // ��������� �� ������� �������
        Node parent;
        
        Node search(Node x, int k){
            if ((x == null) || (k == x.key))
                return x;
            if (k < x.key)
                return search(x.left, k);
            else
                return search(x.right, k);
        }
        
        int function(Node x, int t[])
        {
            int result = 0;
            for (int i = 0; i < t.length; i++)
                search(x,t[i]);
            result = t[0] + (t[1]*(t[2] + (t[3]*(t[4]+t[5]) - (t[6] - t[7]))+t[8]));
            return result;
        }
        
        void insert(Node x, Node z){           // x � ������ ���������, z � ����������� �������
            while (x != null)
                if (z.key > x.key)
                    if (x.right != null)
                        x = x.right;
                    else {
                        z.parent = x;
                        x.right = z;
                        break;
                    }
                else 
                    if (z.key <= x.key)
                        if (x.left != null)
                            x = x.left;
                        else {
                            z.parent = x;
                            x.left = z;
                            break;
                        }
        }
        
        Node minimum(Node x){
            if (x.left == null)
                return x;
            return minimum(x.left);
        }
        
        Node delete(Node root, int z){               // ������ ���������, ��������� ����
            if (root == null) 
                return null;
            if (root.key > z) root.left = delete(root.left, z);
            if (root.key < z) root.right = delete(root.right, z);
            if (root.key == z)
            {
                if ((root.left == null) && (root.right == null))
                    return null;
                if ((root.left == null) && (root.right != null))
                {
                    root.left = root.right.left;
                    root.key = root.right.key;
                    root.right = root.right.right;
                    return root;
                } else
                if ((root.left != null) && (root.right == null))
                {
                    root.right = root.left.right;
                    root.key = root.left.key;
                    root.left = root.left.left;
                    return root;
                } else
                if ((root.left != null) && (root.right != null))
                {
                    Node g = root.right;
                    while (g.left != null) 
                        g = g.left;
                    root.key = g.key;
                    root.right = delete(root.right, g.key);
                }
            }
            return root;
        }
        
        void inorderTraversal(Node x){
            if (x != null){
                inorderTraversal(x.left);
                System.out.print(x.key+" ");
                inorderTraversal(x.right);
            }
        }
        
        int Traversal(Node x, int k,Parameters t){
            if (x != null){
                Traversal(x.left, k, t);
                if (k == x.key)
                    t.y++;
                Traversal(x.right, k, t);
                if (t.y == 0)
                    return -1;
            }
            return 0;
        }
        
        void prefix_export_tree(Node x, String text[],Parameters t)
        {
            text[t.x++] = "\""+x.key+"\"[label=\"{"+x.key+"|{<left>|<right>}}\", fillcolor = white]; \n";
            if (x.left != null)
                text[t.x++] = "\""+x.key+"\": left -> \""+x.left.key+"\";\n";
            if (x.right != null)
                text[t.x++] = "\""+x.key+"\": right -> \""+x.right.key+"\";\n";
            if (x.left != null)
                prefix_export_tree(x.left, text, t);
            if (x.right != null)
                prefix_export_tree(x.right, text, t);
        }
        
        void preorderTraversal(Node x){
            if (x != null){
                System.out.print(x.key+" ");
                preorderTraversal(x.left);
                preorderTraversal(x.right);
            }
        }
        void postorderTraversal(Node x){
            if (x != null){
                postorderTraversal(x.left);
                postorderTraversal(x.right);
                System.out.print(x.key+" ");
            }
        }
       
        static void Read(String text[]) throws FileNotFoundException, IOException{
        System.getProperty("line.separator");
        PrintWriter writer = null;
        try {
            writer = new PrintWriter(new FileWriter("out1.txt"));
            for (int j = 0; j < text.length; j++)
                if (text[j] != null)
                    writer.write(text[j]);
            writer.flush();
        } finally {}
        } 
    }
    static class Parameters
        {
            public int x;
            public int y;
        }
    static void ReadTr(int t[]) throws FileNotFoundException, IOException{
        Scanner scanner = null; 
        PrintWriter writer = null;
        String s1,s2;
        try { 
            scanner = new Scanner(new BufferedReader(new FileReader("standart.txt"))); 
            writer = new PrintWriter(new FileWriter("out.txt")); 
            while (scanner.hasNextLine()) {
                s1 = null;
                s2 = scanner.next();
                for(char i = 'A'; i <= 'I';i++)                
                    if (String.valueOf(i).equals(s2))
                        s1 = Integer.toString(t[i-'A']);
                if (s1 == null)
                    if (!"/".equals(s2))
                        writer.print(s2+' ');
                    else
                        writer.println(' ');
                else
                    writer.print(s1+' ');
            }
            writer.flush();
        } finally {}         
    }
    

    
    public static void main(String[] args) throws IOException {
        int h;
        int bg = 0;
        int f = 0;
        int[] t = new int[9];
        String s1,s2,s3;
        Scanner sc = new Scanner(System.in);
        Node BTree = new Node();
        Node begin = new Node();
        Node biTree = new Node();
        Node bbegin = new Node();
        Parameters p = new Parameters();
        do{
            System.out.println("1 - ������ �������� ���������� �� A �� I;");
            System.out.println("2 - ��������� � ������� �� ����� �������� ������;");
            System.out.println("3 - ����������� ����� ������;");
            System.out.println("4 - ��������� ����� ������;");
            System.out.println("5 - ���������� ����� ������;");
            System.out.println("6 - ������ � �������� �������;");
            System.out.println("7 - ��������� ������� ��� ���������� �������� ������� � ������;");
            System.out.println("8 - �����;");
            s1 = sc.nextLine();
            h = Integer.parseInt(s1);
            switch(h){
                    case 1:
                        System.out.println("������� �������� �� A �� I");
                        for (int i = 0;i <= 8; i++){
                            System.out.print((char)('A'+i)+" = ");
                            s1 = sc.nextLine();
                            t[i] = Integer.parseInt(s1);
                            if (i == 6)
                                t[i] *= -1;
                            Node tmp = new Node();
                            tmp.key = t[i];
                            if (BTree.search(BTree,t[i]) != null)
                                if (biTree.search(BTree,t[i]).key == t[i])
                                {
                                    System.out.println("����� ���� ��� ����������!");
                                    i--;
                                    continue;
                                }
                                else
                                if (BTree.key == t[i])
                                {
                                    System.out.println("����� ���� ��� ����������!");
                                    i--;
                                    continue;
                                }
                            if (i == 0){
                                BTree = tmp;
                                begin = tmp;
                            }
                            else{
                                BTree.search(BTree, t[i]);
                                BTree.insert(BTree,tmp);
                            }
                        }
                        break;
                    case 2:
                        ReadTr(t);
                        String command = "cmd /c start cmd.exe /c dot -Tpng -o out.png out.txt";
                        Process child = Runtime.getRuntime().exec(command);
                        break;
                    case 3:
                        begin.inorderTraversal(begin);
                        System.out.println();
                        break;
                    case 4:
                        begin.preorderTraversal(begin);
                        System.out.println();
                        break;
                    case 5:
                        begin.postorderTraversal(begin);
                        System.out.println();
                        break;
                    case 6:
                        int i = 0;
                        int h1 = 0;
                        do
                        {
                            System.out.println("1 - ������ �������� � �������� ������;");
                            System.out.println("2 - ��������� � ������� �� ����� �������� ������;");
                            System.out.println("3 - ����������� ����� ������;");
                            System.out.println("4 - ��������� ����� ������;");
                            System.out.println("5 - ���������� ����� ������;");
                            System.out.println("6 - ������� ���� �� ��������;");
                            System.out.println("7 - �����;");
                            s2 = sc.nextLine();
                            h1 = Integer.parseInt(s2);
                            switch(h1){
                                case 1:
                                    do{
                                        int h2 = 0;
                                        System.out.println("������� ���� (���������� ����: s): ");
                                        s3 = sc.nextLine();
                                        if ("s".equals(s3))
                                            break;
                                        h2 = Integer.parseInt(s3);
                                        Node tmp = new Node();
                                        tmp.key = h2;
                                        if (biTree.search(biTree,h2) != null)
                                            if (biTree.search(biTree,h2).key == h2)
                                            {
                                                System.out.println("����� ���� ��� ����������!");
                                                break;
                                            }
                                        else
                                            if (biTree.key == h2)
                                            {
                                                System.out.println("����� ���� ��� ����������!");
                                                break;
                                            }
                                        if (i == 0)
                                        {
                                            i++;
                                            biTree = tmp;
                                            bg = tmp.key;
                                        }
                                        else
                                        {
                                            i++;
                                            biTree.insert(biTree,tmp);
                                        }
                                    }while (!"s".equals(s3));
                                    break;
                                case 2:
                                    p.x = 2;
                                    String[] text = new String [3*(i+1)];
                                    text[0] = "digraph TG {  \n";
                                    text[1] = "node[shape=record, style=\"filled\"]; \n";
                                    biTree.prefix_export_tree(biTree,text,p);
                                    text[p.x] = "} ";
                                    biTree.Read(text);
                                    String command1 = "cmd /c start cmd.exe /c dot -Tpng -o out1.png out1.txt";
                                    Process child1 = Runtime.getRuntime().exec(command1);
                                    break;
                                case 3:
                                    biTree.inorderTraversal(biTree);
                                    System.out.println();
                                    break;
                                case 4:
                                    biTree.preorderTraversal(biTree);
                                    System.out.println();
                                    break;
                                case 5:
                                    biTree.postorderTraversal(biTree);
                                    System.out.println();
                                    break;
                                case 6:
                                    int z = 0;
                                    p.y = 0;
                                    do{                             
                                        System.out.println("������� ���� (���������� ����: s): ");
                                        s3 = sc.nextLine();
                                        if ("s".equals(s3))
                                            break;
                                        z = Integer.parseInt(s3);
                                        if (biTree.Traversal(biTree,z,p) == -1)
                                        {
                                            System.out.println("������ ���� �� ����������!");
                                        }
                                        biTree.delete(biTree,z);
                                    }while (!"s".equals(z));
                                    break;
                            }
                        } while (h1 != 7);
                        break;
                    case 7:
                        long l1,l2,l3;
                        System.out.println("�������� ��������� : " + BTree.function(BTree,t));
                        Deque mass = new Deque(9);
                        l3 = mass.Fun(t);
                        System.out.println("����� �� ���������� ����� : " + l3);
                        l1 = System.nanoTime();
                        BTree.function(BTree,t);
                        l2 = System.nanoTime()-l1;
                        System.out.println("����� �� ���������� ������ : " + l2);
                        System.out.println();
                        break;
            }
        } while(h != 8);
    }
    
}
