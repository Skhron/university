unit Main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Grids;

type
// ������ ��� ������� ���������� �������
   Tour = Record
    Country : string[200];
    Amount  : integer;
    Town  : string[20];
    MainLand  : string[20];
    case State : integer of
      1 : (Count,KindV : string[20]);
      2 : (Season, TemO, TemH, Time : string[20] );
      3 : (KindS,Cost : string[20]);
    end;
// ������ ��� ������� ������
    KeyRec = Record
      Amount : integer;
      Index : integer;
    End;

  TForm1 = class(TForm)
    Button1: TButton;
    Label2: TLabel;
    Button2: TButton;
    Button3: TButton;
    StringGrid1: TStringGrid;
    Edit2: TEdit;
    GroupBox1: TGroupBox;
    Button4: TButton;
    Button5: TButton;
    Button6: TButton;
    Button7: TButton;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    GroupBox4: TGroupBox;
    Label1: TLabel;
    GroupBox5: TGroupBox;
    GroupBox7: TGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Button8: TButton;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Button9: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Fill(c:Tobject);
    procedure FillRows(Buff: Tour; i,a:integer; c:Tobject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);
    procedure qSort_Table(min, max: Integer);
    procedure BubbleSort_Table(N:integer);
    procedure Button8Click(Sender: TObject);
    procedure Button9Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  DataFile : file of Tour;              //������������ ���� �������
  KeyArr : array [1..100] of  KeyRec;   // ������ ������
  MaxIndex : integer;                   // ������ ��������u� �������� �����
  SortKeyTime, SortFileTime:int64;      // ����� ����������
  MemmoryV_qsort:int64;
  MemmoryV2:int64;

procedure qSort(var A: array of KeyRec; min, max: Integer);


implementation

uses  AddForm;


{$R *.dfm}
 procedure TForm1.FillRows(Buff:Tour; i,a:integer; c:Tobject);
 begin
    if a = 0 then
    begin
    StringGrid1.Cells[0,i+1]:=IntToStr(i+1);
    StringGrid1.Cells[1,i+1]:= Buff.Country;
    StringGrid1.Cells[2,i+1]:= inttostr(Buff.Amount);
    StringGrid1.Cells[3,i+1]:= Buff.Town;
    StringGrid1.Cells[4,i+1]:=Buff.MainLand;
    end;
    if (Buff.State = 1) and (a = 0)  then
    begin
      StringGrid1.Cells[5,i+1]:= '�������������';
      StringGrid1.Cells[6,i+1]:=Buff.Count;
      StringGrid1.Cells[7,i+1]:=Buff.KindV;
      StringGrid1.Cells[8,i+1]:='-';
      StringGrid1.Cells[9,i+1]:='-';
      StringGrid1.Cells[10,i+1]:='-';
      StringGrid1.Cells[11,i+1]:='-';
      StringGrid1.Cells[12,i+1]:='-';
      StringGrid1.Cells[13,i+1]:='-';
    end;
    if (Buff.State = 2) and (a = 0) then
    begin
      StringGrid1.Cells[5,i+1]:= '�������';
      StringGrid1.Cells[6,i+1]:='-';
      StringGrid1.Cells[7,i+1]:='-';
      StringGrid1.Cells[8,i+1]:=Buff.Season;
      StringGrid1.Cells[9,i+1]:=Buff.TemO;
      StringGrid1.Cells[10,i+1]:=Buff.TemH;
      StringGrid1.Cells[11,i+1]:=Buff.Time;
      StringGrid1.Cells[12,i+1]:='-';
      StringGrid1.Cells[13,i+1]:='-';
    end;
    if (Buff.State = 3) and (a = 0) then
    begin
      StringGrid1.Cells[5,i+1]:= '����������';
      StringGrid1.Cells[6,i+1]:='-';
      StringGrid1.Cells[7,i+1]:='-';
      StringGrid1.Cells[8,i+1]:='-';
      StringGrid1.Cells[9,i+1]:='-';
      StringGrid1.Cells[10,i+1]:='-';
      StringGrid1.Cells[11,i+1]:='-';
      StringGrid1.Cells[12,i+1]:=Buff.KindS;
      StringGrid1.Cells[13,i+1]:=Buff.Cost;
    end;
    if (a = 1) and (Buff.KindS = '������ ����') then
    begin
      StringGrid1.Cells[0,i+1]:=IntToStr(i+1);
      StringGrid1.Cells[1,i+1]:= Buff.Country;
      StringGrid1.Cells[2,i+1]:= inttostr(Buff.Amount);
      StringGrid1.Cells[3,i+1]:= Buff.Town;
      StringGrid1.Cells[4,i+1]:=Buff.MainLand;
      StringGrid1.Cells[5,i+1]:= '����������';
      StringGrid1.Cells[6,i+1]:='-';
      StringGrid1.Cells[7,i+1]:='-';
      StringGrid1.Cells[8,i+1]:='-';
      StringGrid1.Cells[9,i+1]:='-';
      StringGrid1.Cells[10,i+1]:='-';
      StringGrid1.Cells[11,i+1]:='-';
      StringGrid1.Cells[12,i+1]:=Buff.KindS;
      StringGrid1.Cells[13,i+1]:=Buff.Cost;
    end;

 end;

//***********************************qSort*************************************
// ���������� ������� ���������� ��� ������� ������
procedure qSort(var A: array of  KeyRec; min, max: Integer);
var
i, j: Integer;
supp: Integer;
tmp:Keyrec;
begin
supp:=A[max-((max-min) div 2)].Amount;
i:=min; j:=max;
while i<j do
  begin
    while A[i].Amount<supp do i:=i+1;
    while A[j].Amount>supp do j:=j-1;
    if i<=j then
      begin
        tmp:=A[i]; A[i]:=A[j]; A[j]:=tmp;
        i:=i+1; j:=j-1;
      end;
  end;
if min<j then qSort(A, min, j);
if i<max then qSort(A, i, max);
end;

//��� �������
procedure Tform1.qSort_Table(min, max: Integer);
var i, j, supp: Integer;
begin
supp:=StrToInt(StringGrid1.Cells[2,max-((max-min) div 2)]);
i:=min; j:=max;
while i<j do
  begin
    while StrToInt(StringGrid1.Cells[2,i])<supp do i:=i+1;
    while StrToInt(StringGrid1.Cells[2,j])>supp do j:=j-1;
    if i<=j then
      begin
      StringGrid1.Rows[StringGrid1.RowCount]:=StringGrid1.Rows[i];
      StringGrid1.Rows[i]:=StringGrid1.Rows[j];
      StringGrid1.Rows[j]:=StringGrid1.Rows[StringGrid1.RowCount];
      i:=i+1; j:=j-1;
      end;
  end;
if min<j then qSort_Table(min , j);
if i<max then qSort_Table(i, max);
end;
//***********************************qSort*************************************

//*********************************BubbleSort***********************************
 procedure TForm1.BubbleSort_Table(N: Integer);
 var
 bol:boolean;
 v,i:integer;
 begin
  V:=1;
  bol:=true;
  while (bol)and(N>v) do//���� ���� ���������
  begin
 // showmessage('LOL1');
  bol:=false;
  for i:=v to n-1 do//�������� ����� �����
  if StrToInt(StringGrid1.Cells[2,i])>StrToInt(StringGrid1.Cells[2,i+1]) then
  begin
  //showmessage('LOL1');
    StringGrid1.Rows[StringGrid1.RowCount]:=StringGrid1.Rows[i];
    StringGrid1.Rows[i]:=StringGrid1.Rows[i+1];
    StringGrid1.Rows[i+1]:=StringGrid1.Rows[StringGrid1.RowCount];
  bol:=true;
  end;
  dec(n);
  for i:=n downto v+1 do//������ ����
  if StrToInt(StringGrid1.Cells[2,i])<StrToInt(StringGrid1.Cells[2,i-1]) then
  begin
    StringGrid1.Rows[StringGrid1.RowCount]:=StringGrid1.Rows[i];
    StringGrid1.Rows[i]:=StringGrid1.Rows[i-1];
    StringGrid1.Rows[i-1]:=StringGrid1.Rows[StringGrid1.RowCount];
  bol:=true;
  end;
  inc(v);
  //showmessage('LOL2');
  end;
 end;



 procedure BubbleSort(V:integer);
 var
 bol:boolean;
 n,i:integer;
 b: KeyRec;
 begin
 bol:=true;
  n:=1;
  while (bol)and(v>n) do//���� ���� ���������
  begin
  bol:=false;
  for i:=n to v-1 do//�������� ����� �����
  if KeyArr[i].Amount > KeyArr[i+1].Amount then
  begin
  b:=KeyArr[i];
  KeyArr[i]:=KeyArr[i+1];
  KeyArr[i+1]:=b;
  bol:=true;
  end;
  dec(v);
  for i:=v downto n+1 do//������ ����
  if KeyArr[i].Amount < KeyArr[i-1].Amount then begin
  b:=KeyArr[i];
  KeyArr[i]:=KeyArr[i-1];
  KeyArr[i-1]:=b;
  bol:=true;
  end;
  inc(n);
  end;
 end;








//*********************************BubbleSort***********************************

// ���������� ����� �������
procedure Tform1.Fill(c:Tobject);
begin
  with StringGrid1 do
  begin
    Cells[0,0]:= '������';
    Cells[1,0]:= '������';
    Cells[2,0]:= '���-�� �������';
    Cells[3,0]:= '�������';
    Cells[4,0]:= '�������';
    Cells[5,0]:= '������';
    Cells[6,0]:= '���-�� ��������';
    Cells[7,0]:= '�������� ���';
    Cells[8,0]:= '�������� �����';
    Cells[9,0]:= '����������� �������';
    Cells[10,0]:= '����������� ����';
    Cells[11,0]:= '����� ������ �� ������';
    Cells[12,0]:= '��� ������';
    Cells[13,0]:= '����������� ���������';
    end;
end;

// ����� ���� ���������� �����
procedure TForm1.Button1Click(Sender: TObject);
begin
 AddForm.Form3.Edit1.Text:='';
 AddForm.Form3.Edit2.Text:='';
 AddForm.Form3.Edit3.Text:='';
 AddForm.Form3.Edit4.Text:='';
 AddForm.Form3.Edit5.Text:='';
 AddForm.Form3.Edit6.Text:='';
 AddForm.Form3.Edit7.Text:='';
 AddForm.Form3.Edit8.Text:='';
 AddForm.Form3.Edit9.Text:='';
 AddForm.Form3.Edit10.Text:='';
 AddForm.Form3.CheckBox3.Checked:=False;
 AddForm.Form3.CheckBox2.Checked:=False;
 AddForm.Form3.CheckBox1.Checked:=False;
 AddForm.Form3.Show;
end;

//����� ����������� ��������������� ����� � �������
procedure TForm1.Button2Click(Sender: TObject);
var
  Buff : Tour;
  i: Integer;
begin
  label2.Caption:='���������� �����';
  for i := 0 to StringGrid1.RowCount do
    StringGrid1.Rows[i].Clear;
  Fill(StringGrid1);
  AssignFile(DataFile, 'Base.dat');
  Reset(DataFile);
  StringGrid1.RowCount:=FileSize(DataFile)+2;
  MaxIndex:=FileSize(DataFile);
  i:=0;
  while not(EOF(DataFile)) do
  begin
    read(DataFile, Buff);
    FillRows(Buff,i,0,StringGrid1);
    inc(i);
  end;
  closefile(DataFile);
end;

// �������� ������ �� �������
procedure TForm1.Button3Click(Sender: TObject);
var
  BuffDel : Tour;
  i : Integer;
begin
  i:=0;
  if StrToInt(Edit2.Text) > StringGrid1.RowCount then
  begin
    Showmessage('������ �������� �� ����������');
    exit();
  end;
  if MessageDlg('�� ������������� ������ ������� ��������� �������?',
                                mtCustom,[mbYes,mbNo,mbCancel], 0) = mrYes then
  begin
  AssignFile(DataFile, 'Base.dat');
  Reset(DataFile);
  while not(EOF(DataFile)) do
  begin
    Seek(DataFile,StrToInt(Edit2.Text)+i);
    if EOF(DataFile) then
      break;
    read(DataFile, BuffDel);
    Seek(DataFile,StrToInt(Edit2.Text)+i-1);
    Write(DataFile, BuffDel);
    inc(i);
  end;
  Seek(DataFile,FileSize(DataFile)-1);
  truncate(DataFile);
  closefile(DataFile);
  for i := 0 to StringGrid1.RowCount do
    StringGrid1.Rows[i].Clear;
  Fill(StringGrid1);
  //Button4Click(self);  ???
  Button2click(self);
  Edit2.Text := '';
  end;
end;

// ������������ ������� ������
procedure TForm1.Button4Click(Sender: TObject);
var
  Buff : Tour;
  i: Integer;
begin
  i:=1;
  AssignFile(DataFile, 'Base.dat');
  Reset(DataFile);
  while not(EOF(DataFile)) do
  begin
    read(DataFile, Buff);
    KeyArr[i].Amount:=Buff.Amount;
    KeyArr[i].Index:=i;
    inc(i);
  end;
  closefile(DataFile);
  //MaxIndex:=i;
  Button7Click(self);
end;

// ���������� �������
procedure TForm1.Button5Click(Sender: TObject);
var
StartTscValue,EndTscValue:int64;
begin
 if Label2.Caption<>'���������� �����' then
 exit;

//��������� �������
asm
    RDTSC
    MOV     DWORD PTR [StartTscValue], EAX
    MOV     DWORD PTR [StartTscValue+4], EDX
end;
  BubbleSort_Table(MaxIndex);
asm
    RDTSC
    MOV     DWORD PTR [EndTscValue], EAX
    MOV     DWORD PTR [EndTscValue+4], EDX
end;
//��������� �������
label7.Caption:=IntToStr(EndTscValue-StartTscValue);
EndTscValue:=0;
StartTscValue:=0;
//��������� �������
asm
    RDTSC
    MOV     DWORD PTR [StartTscValue], EAX
    MOV     DWORD PTR [StartTscValue+4], EDX
end;
  qSort_Table(1, MaxIndex);
asm
    RDTSC
    MOV     DWORD PTR [EndTscValue], EAX
    MOV     DWORD PTR [EndTscValue+4], EDX
end;
//��������� �������

label5.Caption:=IntToStr(EndTscValue-StartTscValue);
label12.Caption:=IntToStr(2*80)+' byte';
end;

// ���������� ������� ������
procedure TForm1.Button6Click(Sender: TObject);
var
StartTscValue, EndTscValue:int64;
begin
// ��������� �������
asm
    RDTSC
    MOV     DWORD PTR [StartTscValue], EAX
    MOV     DWORD PTR [StartTscValue+4], EDX
end;
  BubbleSort(MaxIndex-1);
asm
    RDTSC
    MOV     DWORD PTR [EndTscValue], EAX
    MOV     DWORD PTR [EndTscValue+4], EDX
end;
// ��������� �������
label11.Caption:=IntToStr(EndTscValue-StartTscValue);
EndTscValue:=0;
StartTscValue:=0;
// ��������� �������
asm
    RDTSC
    MOV     DWORD PTR [StartTscValue], EAX
    MOV     DWORD PTR [StartTscValue+4], EDX
end;

  qSort(KeyArr, 0, MaxIndex-1);

asm
    RDTSC
    MOV     DWORD PTR [EndTscValue], EAX
    MOV     DWORD PTR [EndTscValue+4], EDX
end;
// ��������� �������
  label10.Caption:=IntToStr(EndTscValue-StartTscValue);
  label13.Caption:=inttostr(MaxIndex*sizeof(KeyRec))+' byte';
  Button7Click(self);
end;

// ����� ������� ������
procedure TForm1.Button7Click(Sender: TObject);
var
  i:integer;
begin
  label2.Caption:='������ ������';
  for i := 0 to StringGrid1.RowCount do
    StringGrid1.Rows[i].Clear;
  Stringgrid1.Cells[0,0]:= '�����';
  Stringgrid1.Cells[1,0]:= '����';
  Stringgrid1.Cells[2,0]:= '������';
  for i := 1 to MaxIndex do
  begin
    Stringgrid1.Cells[0,i]:= inttostr(i) ;
    Stringgrid1.Cells[1,i]:= inttostr(Keyarr[i].Amount);
    Stringgrid1.Cells[2,i]:= IntToStr(Keyarr[i].Index);
  end;
end;


procedure TForm1.Button8Click(Sender: TObject);
var
  i:integer;
  Buff: Tour;
begin
  AssignFile(DataFile, 'Base.dat');
  Reset(DataFile);
  Fill(StringGrid1);
  for I := 0 to MaxIndex-1 do
  begin
    Seek(DataFile,KeyArr[i+1].Index-1);
    read(DataFile,Buff);
    FillRows(Buff,i,0,StringGrid1);
  end;

  Closefile(DataFile);

end;

procedure TForm1.Button9Click(Sender: TObject);
var
  Buff : Tour;
  i: Integer;
begin
  label2.Caption:='���������� �����';
  for i := 0 to StringGrid1.RowCount do
    StringGrid1.Rows[i].Clear;
  Fill(StringGrid1);
  AssignFile(DataFile, 'Base.dat');
  Reset(DataFile);
  StringGrid1.RowCount:=FileSize(DataFile)+2;
  MaxIndex:=FileSize(DataFile);
  i:=0;
  while not(EOF(DataFile)) do
  begin
    read(DataFile, Buff);
    FillRows(Buff,i,1,StringGrid1);
    if StringGrid1.Cells[0,i+1] <> '' then
      inc(i);
  end;
  closefile(DataFile);
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  Fill(StringGrid1);
  AssignFile(DataFile, 'Base.dat');
  reset(DataFile);
  StringGrid1.RowCount:=FileSize(DataFile)+2;
  MaxIndex:= FileSize(DataFile);
  closefile(DataFile);
end;

end.
