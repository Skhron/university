unit ConfirmForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TForm2 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Label1: TLabel;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form2: TForm2;

implementation

uses  Main;

{$R *.dfm}

procedure TForm2.Button1Click(Sender: TObject);
begin
  Main.Form1.Button3.ModalResult:=mrYes;
  close;
end;

procedure TForm2.Button2Click(Sender: TObject);
begin
  Main.Form1.Button3.ModalResult:=mrNo;
  close;
end;

procedure TForm2.Button3Click(Sender: TObject);
begin
  Main.Form1.Button3.ModalResult:=mrCancel;
  close;
end;

end.
