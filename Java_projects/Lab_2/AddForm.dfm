object Form3: TForm3
  Left = 0
  Top = 0
  Caption = #1044#1086#1073#1072#1074#1083#1077#1085#1080#1077' '#1085#1086#1074#1086#1081' '#1079#1072#1087#1080#1089#1080' '#1074' '#1073#1072#1079#1091
  ClientHeight = 302
  ClientWidth = 590
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 13
    Width = 41
    Height = 13
    Caption = #1057#1090#1088#1072#1085#1072':'
  end
  object Label2: TLabel
    Left = 240
    Top = 16
    Width = 111
    Height = 13
    Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086' '#1078#1080#1090#1077#1083#1077#1081':'
  end
  object Label3: TLabel
    Left = 16
    Top = 59
    Width = 47
    Height = 13
    Caption = #1057#1090#1086#1083#1080#1094#1072':'
  end
  object Label4: TLabel
    Left = 240
    Top = 59
    Width = 48
    Height = 13
    Caption = #1052#1072#1090#1077#1088#1080#1082':'
  end
  object Label5: TLabel
    Left = 143
    Top = 117
    Width = 116
    Height = 13
    Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086' '#1086#1073#1098#1077#1082#1090#1086#1074':'
  end
  object Label6: TLabel
    Left = 280
    Top = 163
    Width = 117
    Height = 13
    Caption = #1058#1077#1084#1087#1077#1088#1072#1090#1091#1088#1072' '#1074#1086#1079#1076#1091#1093#1099':'
  end
  object Label7: TLabel
    Left = 280
    Top = 117
    Width = 84
    Height = 13
    Caption = #1054#1089#1085#1086#1074#1085#1086#1081' '#1089#1077#1079#1086#1085':'
  end
  object Label8: TLabel
    Left = 280
    Top = 209
    Width = 100
    Height = 13
    Caption = #1058#1077#1084#1087#1077#1088#1072#1090#1091#1088#1072' '#1074#1086#1076#1099':'
  end
  object Label9: TLabel
    Left = 280
    Top = 255
    Width = 129
    Height = 13
    Caption = #1042#1088#1077#1084#1103' '#1087#1086#1083#1077#1090#1072' '#1076#1086' '#1089#1090#1088#1072#1085#1099':'
  end
  object Label10: TLabel
    Left = 143
    Top = 163
    Width = 75
    Height = 13
    Caption = #1054#1089#1085#1086#1074#1085#1086#1081' '#1074#1080#1076':'
  end
  object Label11: TLabel
    Left = 416
    Top = 117
    Width = 61
    Height = 13
    Caption = #1042#1080#1076' '#1089#1087#1086#1088#1090#1072':'
  end
  object Label12: TLabel
    Left = 416
    Top = 163
    Width = 169
    Height = 13
    Caption = #1052#1080#1085#1080#1084#1072#1083#1100#1085#1072#1103' '#1089#1090#1086#1080#1084#1086#1089#1090#1100' '#1086#1090#1076#1099#1093#1072':'
  end
  object GroupBox1: TGroupBox
    Left = 8
    Top = 108
    Width = 129
    Height = 96
    Caption = #1054#1089#1085#1086#1074#1085#1086#1081' '#1074#1080#1076' '#1090#1091#1088#1080#1079#1084#1072':'
    TabOrder = 7
    object CheckBox3: TCheckBox
      Left = 8
      Top = 69
      Width = 97
      Height = 17
      Caption = #1057#1087#1086#1088#1090#1080#1074#1085#1099#1081
      TabOrder = 0
      OnClick = CheckBox3Click
    end
  end
  object Edit1: TEdit
    Left = 16
    Top = 32
    Width = 145
    Height = 21
    TabOrder = 0
  end
  object Edit2: TEdit
    Left = 16
    Top = 78
    Width = 161
    Height = 21
    TabOrder = 1
  end
  object Edit3: TEdit
    Left = 240
    Top = 32
    Width = 145
    Height = 21
    NumbersOnly = True
    TabOrder = 2
  end
  object Edit4: TEdit
    Left = 240
    Top = 75
    Width = 161
    Height = 21
    TabOrder = 3
  end
  object Edit5: TEdit
    Left = 143
    Top = 136
    Width = 121
    Height = 21
    NumbersOnly = True
    TabOrder = 4
  end
  object CheckBox1: TCheckBox
    Left = 16
    Top = 154
    Width = 97
    Height = 17
    Caption = #1055#1083#1103#1078#1085#1099#1081
    TabOrder = 5
    OnClick = CheckBox1Click
  end
  object CheckBox2: TCheckBox
    Left = 16
    Top = 131
    Width = 97
    Height = 17
    Caption = #1069#1082#1089#1082#1091#1088#1089#1080#1086#1085#1085#1099#1081
    TabOrder = 6
    OnClick = CheckBox2Click
  end
  object Edit6: TEdit
    Left = 280
    Top = 182
    Width = 121
    Height = 21
    NumbersOnly = True
    TabOrder = 8
  end
  object Edit7: TEdit
    Left = 280
    Top = 228
    Width = 121
    Height = 21
    NumbersOnly = True
    TabOrder = 9
  end
  object Edit8: TEdit
    Left = 280
    Top = 136
    Width = 121
    Height = 21
    TabOrder = 10
  end
  object Button1: TButton
    Left = 16
    Top = 250
    Width = 75
    Height = 25
    Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100
    TabOrder = 11
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 128
    Top = 250
    Width = 75
    Height = 25
    Caption = #1054#1090#1084#1077#1085#1072
    TabOrder = 12
    OnClick = Button2Click
  end
  object ComboBox1: TComboBox
    Left = 143
    Top = 182
    Width = 121
    Height = 21
    ItemIndex = 0
    TabOrder = 13
    Text = #1055#1088#1080#1088#1086#1076#1072
    Items.Strings = (
      #1055#1088#1080#1088#1086#1076#1072
      #1048#1089#1090#1086#1088#1080#1103
      #1048#1089#1082#1091#1089#1089#1090#1074#1086)
  end
  object Edit9: TEdit
    Left = 280
    Top = 274
    Width = 121
    Height = 21
    NumbersOnly = True
    TabOrder = 14
  end
  object ComboBox2: TComboBox
    Left = 416
    Top = 136
    Width = 97
    Height = 21
    ItemIndex = 0
    TabOrder = 15
    Text = #1043#1086#1088#1085#1099#1077' '#1083#1099#1078#1080
    Items.Strings = (
      #1043#1086#1088#1085#1099#1077' '#1083#1099#1078#1080
      #1057#1077#1088#1092#1080#1085#1075
      #1042#1086#1089#1093#1086#1078#1076#1077#1085#1080#1077)
  end
  object Edit10: TEdit
    Left = 416
    Top = 182
    Width = 121
    Height = 21
    NumbersOnly = True
    TabOrder = 16
  end
end
