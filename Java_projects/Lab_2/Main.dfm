object Form1: TForm1
  Left = 0
  Top = 0
  Caption = #1056#1072#1073#1086#1090#1072' '#1089' '#1079#1072#1087#1080#1089#1103#1084#1080
  ClientHeight = 524
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 24
    Top = 21
    Width = 4
    Height = 16
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsItalic]
    ParentFont = False
  end
  object Label3: TLabel
    Left = 423
    Top = 336
    Width = 235
    Height = 13
    Caption = #1056#1077#1079#1091#1083#1100#1090#1072#1090#1099' '#1087#1086' '#1074#1088#1077#1084#1077#1085#1080'('#1074' '#1090#1072#1082#1090#1072#1093' '#1087#1088#1086#1094#1077#1089#1089#1086#1088#1072')'
  end
  object Label4: TLabel
    Left = 424
    Top = 374
    Width = 105
    Height = 13
    Caption = #1041#1099#1089#1090#1088#1072#1103' '#1089#1086#1088#1090#1080#1088#1086#1074#1082#1072
  end
  object Label5: TLabel
    Left = 561
    Top = 374
    Width = 4
    Height = 14
    Caption = '-'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label6: TLabel
    Left = 425
    Top = 406
    Width = 40
    Height = 13
    Caption = #1064#1077#1081#1082#1077#1088
  end
  object Label7: TLabel
    Left = 561
    Top = 406
    Width = 4
    Height = 14
    Caption = '-'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label8: TLabel
    Left = 561
    Top = 355
    Width = 35
    Height = 13
    Caption = #1047#1072#1087#1080#1089#1080
  end
  object Label9: TLabel
    Left = 657
    Top = 355
    Width = 34
    Height = 13
    Caption = #1050#1083#1102#1095#1080
  end
  object Label10: TLabel
    Left = 657
    Top = 374
    Width = 4
    Height = 14
    Caption = '-'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label11: TLabel
    Left = 657
    Top = 406
    Width = 4
    Height = 14
    Caption = '-'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label12: TLabel
    Left = 559
    Top = 456
    Width = 4
    Height = 14
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label13: TLabel
    Left = 657
    Top = 456
    Width = 4
    Height = 14
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label14: TLabel
    Left = 423
    Top = 456
    Width = 122
    Height = 13
    Caption = #1048#1089#1087#1086#1083#1100#1079#1086#1074#1072#1085#1085#1072#1103' '#1087#1072#1084#1103#1090#1100
  end
  object GroupBox7: TGroupBox
    Left = 422
    Top = 231
    Width = 174
    Height = 64
    Caption = #1057#1086#1088#1090#1080#1088#1086#1074#1072#1090#1100' '#1084#1072#1089#1089#1080#1074' '#1082#1083#1102#1095#1077#1081
    TabOrder = 14
  end
  object GroupBox5: TGroupBox
    Left = 423
    Top = 159
    Width = 173
    Height = 66
    Caption = #1057#1086#1088#1090#1080#1088#1086#1074#1072#1090#1100' '#1090#1072#1073#1083#1080#1094#1091
    TabOrder = 13
  end
  object GroupBox2: TGroupBox
    Left = 602
    Top = 132
    Width = 174
    Height = 163
    Caption = #1056#1072#1073#1086#1090#1072' '#1089' '#1084#1072#1089#1089#1080#1074#1086#1084' '#1082#1083#1102#1095#1077#1081
    TabOrder = 10
  end
  object GroupBox4: TGroupBox
    Left = 423
    Top = 87
    Width = 173
    Height = 66
    Caption = #1056#1072#1073#1086#1090#1072' '#1089' '#1092#1072#1081#1083#1086#1084
    TabOrder = 12
  end
  object GroupBox3: TGroupBox
    Left = 423
    Top = 21
    Width = 173
    Height = 60
    Caption = #1044#1086#1073#1072#1074#1083#1077#1085#1080#1077' '#1079#1072#1087#1080#1089#1080
    TabOrder = 11
  end
  object GroupBox1: TGroupBox
    Left = 602
    Top = 21
    Width = 174
    Height = 105
    Caption = #1059#1076#1072#1083#1077#1085#1080#1077' '#1101#1083#1077#1084#1077#1085#1090#1072
    TabOrder = 5
    object Label1: TLabel
      Left = 32
      Top = 20
      Width = 126
      Height = 13
      Caption = #1053#1086#1084#1077#1088' '#1091#1076#1072#1083#1103#1077#1084#1086#1081' '#1079#1072#1087#1080#1089#1080
    end
  end
  object Button1: TButton
    Left = 431
    Top = 41
    Width = 154
    Height = 25
    Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1079#1072#1087#1080#1089#1100
    TabOrder = 0
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 431
    Top = 113
    Width = 154
    Height = 25
    Caption = #1042#1099#1074#1077#1089#1090#1080' '#1076#1072#1085#1085#1099#1077' '#1080#1079' '#1092#1072#1081#1083#1072
    TabOrder = 1
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 623
    Top = 87
    Width = 107
    Height = 25
    Caption = #1059#1076#1072#1083#1080#1090#1100' '#1079#1072#1087#1080#1089#1100
    TabOrder = 2
    OnClick = Button3Click
  end
  object StringGrid1: TStringGrid
    Left = 8
    Top = 21
    Width = 393
    Height = 476
    ColCount = 14
    RowCount = 50
    TabOrder = 3
    ColWidths = (
      64
      64
      64
      64
      64
      64
      64
      89
      102
      64
      64
      64
      64
      64)
  end
  object Edit2: TEdit
    Left = 623
    Top = 60
    Width = 121
    Height = 21
    TabOrder = 4
  end
  object Button4: TButton
    Left = 605
    Top = 159
    Width = 155
    Height = 25
    Caption = #1057#1086#1079#1076#1072#1090#1100' '#1084#1072#1089#1089#1080#1074' '#1082#1083#1102#1095#1077#1081
    TabOrder = 6
    OnClick = Button4Click
  end
  object Button5: TButton
    Left = 431
    Top = 183
    Width = 154
    Height = 25
    Caption = #1057#1086#1088#1090#1080#1088#1086#1074#1082#1072
    TabOrder = 7
    OnClick = Button5Click
  end
  object Button6: TButton
    Left = 430
    Top = 256
    Width = 155
    Height = 25
    Caption = #1057#1086#1088#1090#1080#1088#1086#1074#1082#1072
    TabOrder = 8
    OnClick = Button6Click
  end
  object Button7: TButton
    Left = 605
    Top = 207
    Width = 155
    Height = 25
    Caption = #1042#1099#1074#1077#1089#1090#1080' '#1084#1072#1089#1089#1080#1074' '#1082#1083#1102#1095#1077#1081
    TabOrder = 9
    OnClick = Button7Click
  end
  object Button8: TButton
    Left = 605
    Top = 256
    Width = 155
    Height = 25
    Caption = #1042#1099#1074#1077#1089#1090#1080' '#1079#1072#1087#1080#1089#1080' '#1087#1086' '#1084#1072#1089#1089#1080#1074#1091
    TabOrder = 15
    OnClick = Button8Click
  end
  object Button9: TButton
    Left = 512
    Top = 301
    Width = 165
    Height = 29
    Caption = #1042#1099#1074#1086#1076' '#1087#1086' '#1075#1086#1088#1085#1099#1084' '#1083#1099#1078#1072#1084
    TabOrder = 16
    OnClick = Button9Click
  end
end
