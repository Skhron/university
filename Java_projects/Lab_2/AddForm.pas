unit AddForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TForm3 = class(TForm)
    Edit1: TEdit;
    Edit2: TEdit;
    Edit3: TEdit;
    Edit4: TEdit;
    Edit5: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    CheckBox1: TCheckBox;
    CheckBox2: TCheckBox;
    GroupBox1: TGroupBox;
    Edit6: TEdit;
    Edit7: TEdit;
    Edit8: TEdit;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Button1: TButton;
    Button2: TButton;
    CheckBox3: TCheckBox;
    ComboBox1: TComboBox;
    Label9: TLabel;
    Edit9: TEdit;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    ComboBox2: TComboBox;
    Edit10: TEdit;
    procedure CheckBox2Click(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    //procedure FormCreate(Sender: TObject);
    procedure CheckBox3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form3: TForm3;

implementation

uses  Main;

{$R *.dfm}

procedure TForm3.Button1Click(Sender: TObject);
var
  Buff: Main.Tour;
  i:integer;
begin
  if (CheckBox1.Checked = false) and (CheckBox2.Checked = false)
  and (CheckBox3.Checked = false) then
  begin
    showmessage('������ ���� ������ �������� ��� �������!');
    exit;
  end;

  if (Edit1.Text = '') or  (Edit2.Text = '') or  (Edit3.Text = '')
  or  (Edit4.Text = '') then
  begin
    showmessage('��� ��������� ���� ������ ���� ���������!');
    exit;
  end;

  if (CheckBox2.Checked = true) and (Edit5.Text = '') then
  begin
    showmessage('��� ��������� ���� ������ ���� ���������!');
    exit;
  end;

  if (CheckBox1.Checked = true) and ((Edit6.Text = '') or (Edit7.Text = '') or
  (Edit8.Text = '') or (Edit9.Text = '')) then
  begin
    showmessage('��� ��������� ���� ������ ���� ���������!');
    exit;
  end;

  if (CheckBox3.Checked = true) and (Edit10.Text = '') then
  begin
    showmessage('��� ��������� ���� ������ ���� ���������!');
    exit;
  end;


Buff.Country:=Edit1.Text;
Buff.Town:=Edit2.Text;
Buff.MainLand:=Edit4.Text;
Buff.Amount:=strtoint(Edit3.Text);
if CheckBox2.Checked then
begin
  Buff.State:=1;
  Buff.Count:=Edit5.Text;
  Buff.KindV:=ComboBox1.Items[ComboBox1.ItemIndex];
  //Buff.TemO:='-';
  //Buff.TemH:='-';
  //Buff.Season:='-';
  //Buff.Time:='-';
  //Buff.Cost:='-';
  //Buff.KindS:='-';
end;
if CheckBox1.Checked then
begin
    Buff.State:=2;
    //Buff.Count:='-';
    //Buff.KindV:='-';
    Buff.TemO:=Edit6.Text;
    Buff.TemH:=Edit7.Text;
    Buff.Season:=Edit8.Text;
    Buff.Time:=Edit9.Text;
    //Buff.Cost:='-';
    //Buff.KindS:='-';
end;
if CheckBox3.Checked then
begin
  Buff.State:=3;
  //Buff.Count:='-';
  //Buff.KindV:='-';
  //Buff.TemO:='-';
  //Buff.TemH:='-';
  //Buff.Season:='-';
  //Buff.Time:='-';
  Buff.Cost:=Edit10.Text;
  Buff.KindS:=ComboBox2.Items[ComboBox2.ItemIndex];
end;
    AssignFile(DataFile, 'Base.dat');
    Reset(DataFile);
    Seek(DataFile,FileSize(DataFile));
    Write(Main.DataFile, Buff);
    closefile(DataFile);
    for i := 0 to Main.Form1.StringGrid1.RowCount do
    Main.Form1.StringGrid1.Rows[i].Clear;
    Main.Form1.Fill(Main.Form1.StringGrid1);
    Main.Form1.Button2click(self);
    AddForm.Form3.Close;

end;

procedure TForm3.Button2Click(Sender: TObject);
begin
  AddForm.Form3.Close;
end;

procedure TForm3.CheckBox2Click(Sender: TObject);
begin
CheckBox1.Checked := False;
CheckBox3.Checked := False;
Edit5.Enabled := True;
Edit6.Enabled := False;
Edit7.Enabled := False;
Edit8.Enabled := False;
Edit9.Enabled := False;
Edit10.Enabled := False;
ComboBox1.Enabled := True;
ComboBox2.Enabled := False;
end;

procedure TForm3.CheckBox1Click(Sender: TObject);
begin
CheckBox2.Checked := False;
CheckBox3.Checked := False;
Edit5.Enabled := False;
Edit6.Enabled := True;
Edit7.Enabled := True;
Edit8.Enabled := True;
Edit9.Enabled := True;
Edit10.Enabled := False;
ComboBox1.Enabled := False;
ComboBox2.Enabled := False;
end;

procedure TForm3.CheckBox3Click(Sender: TObject);
begin
CheckBox1.Checked := False;
CheckBox2.Checked := False;
Edit5.Enabled := False;
Edit6.Enabled := False;
Edit7.Enabled := False;
Edit8.Enabled := False;
Edit9.Enabled := False;
Edit10.Enabled := True;
ComboBox1.Enabled := False;
ComboBox2.Enabled := True;
end;

end.
