/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package queue;

import java.util.Random;

/**
 *
 * @author Sh0N
 */
public class Queue {
    
    static class Stack {
        int tail;
        int head;
        double[] data;
        
        Stack(int capacity) {
            data = new double [capacity];
            head = capacity;
        }
        
        void push(double value) {
            if (tail != head)
                data[tail++] = value;
            else
            {
                head += 100;
                data = new double [head];
            }
        }
        
        void pop() {
            if (tail > 0) {
                for(int i = 0; i < tail-1; i++)
                    data[i]=data[i+1];
                tail--;
            }
        }        
    }
    
    static class Item {
        Item next;              // ��������� �� ��������� �������
        double data;            // ������
    }
    
    static class List {
        private Item head;       // ��������� �� ������ �������
        private Item tail;       // ��������� �� ��������� �������
        
        void addBack(double data) {          //���������� � ����� ������
            Item a = new Item();          
            a.data = data;
            if (head == null)           
            {                           
                head = a;               
                tail = a;
            } 
            else {
                tail.next = a;          //"������" ��������� ������� ��������� �� �����
                tail = a;               //� ��������� �� ��������� ������� ���������� ����� ������ ��������
            }
        }
        void delEl() {         //�������� ������� ��������
            if(head == null){         //���� ����
                //System.out.println("���� ����!");        
            }
            else {
                head = head.next;
            }
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Random random = new Random();
        int t = 0;
        double time = 0;
        double time1 = 0;
        double r1,r2,r3,r4;
        int t1 = 1,t2 = 5,t3 = 0,t4 = 3,t5 = 0,t6 = 4,t7 = 0,t8 = 1;
        int res1 =0;
        int res2 =0;
        int res3 =0;
        int res4 =0;
        int res5 =0;
        int res6 =0;
        double i = 0;
        Stack stack = new Stack(100);
        List list = new List();
        //while (res1 < 1000) {
            r1 = random.nextDouble()*(t2-t1) + t1;
            r2 = random.nextDouble()*(t4-t3) + t3;
            r3 = random.nextDouble()*(t6-t5) + t5;
            r4 = random.nextDouble()*(t8-t7) + t7;
            r3+=r1;
            r4+=r2;
            while (res1 < 1000)  {
                if ((res1 != 0) && (stack.tail == 0))
                    r3+=0.05;
                if (r4 < i)
                    r4 += 0.1;
                if (r2 < i)
                    r2 += 0.1;
                if ((stack.tail != 0) && (r3 >= i) && (r3 < i+0.5)) {
                    stack.pop();
                    r3 += random.nextDouble()*(t6-t5) + t5;
                    res6++;
                    res1++;
                    if (res1 % 100 == 0) {
                        int x = 0,y = 0;
                        Item temp = list.head;
                        for (int j = 0; j < stack.tail; j++)
                            if (stack.data[j] != 0)
                                y++;
                        while (temp != null)
                            {
                                x++;
                                temp = temp.next;
                            }
                        time1 = i - time1;
                        System.out.println(res1+" ������:");
                        System.out.println("������ � ������� 1: "+y);
                        System.out.println("������ � ������� 2: "+x);
                        System.out.println("��������/�������� ������ � ������� 1 - "+res3+"/"+res6);
                        System.out.println("��������/�������� ������ � ������� 2 - "+res4+"/"+res5);
                        System.out.println("������� ����� ���������� � 1-�� ������� - "+((stack.data[stack.tail]-stack.data[0]+1)/(y+2)));
                        System.out.println("������� ����� ���������� � 2-�� ������� - "+((list.tail.data - list.head.data)));
                        //res3 = 0;
                        //res4 = 0;
                        //res5 = 0;
                        //res6 = 0;
                    }
                    if (res1 == 1000) {
                        time += 0.05;
                        break;
                    }
                }
                
                 if ((r1 >= i) && (r1 < i+0.05)) {
                    res3++;
                    stack.push(r1);
                    r1 += random.nextDouble()*(t2-t1) + t1;
                }
                if ((r2 >= i) && (r2 < i+0.05)) {
                    res4++;
                    list.addBack(r2);
                    r2 += random.nextDouble()*(t4-t3) + t3;
                }

                if ((stack.tail == 0) && (list.head != null) && (r4 >= i) && (r4 < i+0.05)) {
                    res5++;
                    //if (list.tail.data < r4)
                    //   t++;
                    //else {
                        //System.out.println("r4 ="+r4);
                        list.delEl();
                        r4 += random.nextDouble()*(t8-t7) + t7;
                        res2++;
                    //}
                } 
                i += 0.05;
            }
            time += i;
        //}
        int temp = 0;
        for (int j = 0; j < stack.tail; j++)
            if (stack.data[j] != 0)
                temp++;
        Item tmp = list.head;
        while (tmp != null)
        {
            t++;
            tmp = tmp.next;
        }
        System.out.println("����� ������������� - "+time);
        System.out.println("�������� ������ 1-�� - "+(temp+res1));
        System.out.println("�������� ������ 1-�� - "+res1);
        System.out.println("�������� ������ 2-�� - "+(t+res2));
        System.out.println("�������� ������ 2-�� - "+(res2));
        System.out.println("������� �������� - "+(res1+res2));
    }
    
}
