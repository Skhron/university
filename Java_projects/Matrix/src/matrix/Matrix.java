package matrix;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

/**
 *
 * @author Sh0N
 */
public class Matrix {
    
    static class Item {
        Item next;              // ��������� �� ��������� �������
        int data;            // ������
    }
    static class List {
        private Item head;       // ��������� �� ������ �������
        private Item tail;       // ��������� �� ��������� �������
        
        void addBack(int data) {          //���������� � ����� ������
            Item a = new Item();          
            a.data = data;
            if (head == null)           
            {                           
                head = a;               
                tail = a;
            } 
            else {
                tail.next = a;          //"������" ��������� ������� ��������� �� �����
                tail = a;               //� ��������� �� ��������� ������� ���������� ����� ������ ��������
            }
        }
        void delEl() {         //�������� ������� ��������
            if(head == null){         //���� ����
                //System.out.println("���� ����!");        
            }
            else {
                head = head.next;
            }
        }
    }
    static class Array {
        int[] A;
        int[] JA;
        List IA;
        
        Array(int capacity[][]) {
            int t = 0;
            int n = 0;
            IA = new List();
            for(int i = 0; i < capacity.length;i++)
                for(int j = 0; j < capacity[0].length;j++)
                    if (capacity[i][j] != 0)
                        n++;
            A = new int [n];
            JA = new int [n];
            IA.addBack(t);
            for(int i = 0; i < capacity.length;i++){
                for(int j = 0; j < capacity[0].length;j++)
                    if (capacity[i][j] != 0){
                        JA[t] = j;
                        A[t] = capacity[i][j];
                        t++;
                    }
                IA.addBack(t);
            }
            IA.addBack(t);
        }
        
        long Sum(Array l1,Array l2,int n){
            Item tmp1 = l1.IA.head;
            Item tmp2 = l2.IA.head;
            int len1 = 0;
            int len2 = 0;
            int max = 0;
            int min = 0;
            int t1 = 0;
            Array p1,p2;
            IA.head = null;
            IA.addBack(t1);
            if (A.length == 0)
                return 0;
            for(int i = 0; i < A.length;i++){
                A[i] = 0;
                JA[i] = -1;
            }
            long f1,f2;
            f1 = System.nanoTime();
            while ((tmp2.next.next != null) || (tmp1.next.next != null))
            {
                if (((tmp1.next.data-tmp1.data) >= (tmp2.next.data-tmp2.data)))
                {
                    max = tmp1.data;
                    len1 = tmp1.next.data-tmp1.data;
                    min = tmp2.data;
                    len2 = tmp2.next.data-tmp2.data;
                    p1 = l1;
                    p2 = l2;
                    if ((len1 != 0) && (len2 != 0))
                        if (l1.JA[tmp1.next.data-1] < l2.JA[tmp2.next.data-1])
                        {
                            max = tmp2.data;
                            len1 = tmp2.next.data-tmp2.data;
                            min = tmp1.data;
                            len2 = tmp1.next.data-tmp1.data;
                            p2 = l1;
                            p1 = l2;
                        }   
                }
                else
                {
                    max = tmp2.data;
                    len1 = tmp2.next.data-tmp2.data;
                    min = tmp1.data;
                    len2 = tmp1.next.data-tmp1.data;
                    p2 = l1;
                    p1 = l2;
                }
                for(int i = max; i < max + len1; i++)
                {
                    while((min < len2+min) && (p2.JA[min] < p1.JA[i]))
                    {
                        A[t1] = p2.A[min];
                        JA[t1] = p2.JA[min];
                        t1++;
                        min++;
                        len2--;
                    }
                    if ((min < len2+min) && (p2.JA[min] == p1.JA[i]))                                                                                         
                    {
                        if ((p2.A[min]+p1.A[i]) == 0)
                        {
                            min++;
                            len2--;
                            continue;
                        }
                        A[t1] = p2.A[min]+p1.A[i];
                        JA[t1] = p2.JA[min];
                        min++;
                        len2--;
                        t1++;
                        continue;
                    }
                    A[t1] = p1.A[i];
                    JA[t1] = p1.JA[i];
                    t1++;
                }        
                IA.addBack(t1);
                tmp1 = tmp1.next;
                tmp2 = tmp2.next;
            }
            f2 = System.nanoTime()-f1;
            IA.addBack(t1);
            return f2;
        }
        
        void print_arr(){
            System.out.println("A: ");
            for(int i = 0; i < A.length; i++){
                System.out.print(A[i]+" ");
            }
            System.out.println();
            System.out.println("JA: ");
            for(int i = 0; i < JA.length; i++){
                System.out.print(JA[i]+" ");
            }     
            System.out.println();
            System.out.println("IA: ");
            Item tmp = IA.head;
            while (tmp != null) {
                System.out.print(tmp.data+" ");
                tmp = tmp.next;
            }
            System.out.println();
        }
    }
    public static void main(String[] args) {
        long fm1, fm2;
        long tm1, tm2;
        String s1,s2;
        int n = 0,m = 0;
        int n1 = 0, m1 = 0;
        int k = 0, f = 0;
        int[][] a = null,b = null;
        int[][] c;
        Scanner sc = new Scanner(System.in);       
        Scanner scanner = null;
        BufferedReader in = null;
        System.out.print("������� ���� � �������� �: \n");
        s1 = sc.nextLine();
        try { 
            scanner = new Scanner(new BufferedReader(new FileReader(s1)));
            if (!scanner.hasNextInt()){
                System.out.println("�������� ������ �������!");
                System.exit(1);
            }
            n = scanner.nextInt();
            m = scanner.nextInt();
            if ((n <= 0) || (m <= 0)){
                System.out.println("�������� ������ �������!");
                System.exit(1);
            }
            a = new int [n][m];
            while (scanner.hasNextInt()) { 
                if (f == m){
                    f = 0;
                    k++;
                }
                a[k][f++] = scanner.nextInt();
            }
            if ((n+m) != (f+k+1)){
                System.out.println("�������� ������ �������!");
                System.exit(1);
            }
        } 
        catch (FileNotFoundException fnfe){
            System.out.print("���� �� ������! \n");
            System.exit(1);
        }
        for (int i = 0; i < n; i ++) {
           for (int j = 0; j < m; j ++)
               System.out.print(a[i][j] + " ");
           System.out.print("\n");
        }
        k = 0;
        f = 0;
        Array a1 = new Array(a);
        a1.print_arr();
        System.out.print("������� ���� � �������� B: \n");
        s2 = sc.nextLine();
        try { 
            scanner = new Scanner(new BufferedReader(new FileReader(s2)));
            if (!scanner.hasNextInt()){
                System.out.println("�������� ������ �������!");
                System.exit(1);
            }
            n1 = scanner.nextInt();
            m1 = scanner.nextInt();
            if ((n1 != n) || (m1 != m)){
                System.out.println("�������� ������ �������!");
                System.exit(1);
            }
            b = new int [n][m];
            while (scanner.hasNextInt()) { 
                if (f == m){
                    f = 0;
                    k++;
                }
                b[k][f++] = scanner.nextInt();
            }
            if ((n+m) != (f+k+1)){
                System.out.println("�������� ������ �������!");
                System.exit(1);
            }
        } 
        catch (FileNotFoundException fnfe){
            System.out.print("���� �� ������! \n");
            System.exit(1);
        }
        Array a2 = new Array(b);
        for (int i = 0; i < n; i ++) {
           for (int j = 0; j < m; j ++)
               System.out.print(b[i][j] + " ");
           System.out.print("\n");
        }
        a2.print_arr();
        Runtime u2 = Runtime.getRuntime();
        u2.gc();
        fm1 = u2.freeMemory();
        c = new int [n][m];
        fm2 = u2.freeMemory();
        System.out.print("������������ ������ A � B: \n");
        long l1,l2,l3;
        l1 = System.nanoTime();
        for (int i = 0; i < n; i++)
            for (int j = 0; j < m; j++)
                c[i][j] += a[i][j] + b[i][j];
        l2 = System.nanoTime()-l1;
        for (int i = 0; i < n; i++){
            for (int j = 0; j < m; j++)
                System.out.print(c[i][j]+" ");
           System.out.println(); 
        }
        Runtime u3 = Runtime.getRuntime();
        u3.gc();
        tm1 = u3.freeMemory();
        Array a3 = new Array(c); 
        tm2 = u3.freeMemory();
        Item tmp1 = a3.IA.head;
        while(a3.IA.head != null){
            a3.IA.delEl();
        }
        l3 = a3.Sum(a1,a2,n);
        a3.print_arr();
        System.out.println("����� ������������ ������� �������: "+l2);
        System.out.println("����� ������������ ����������� �������: "+l3);
        System.out.println("����� ������ �� ������� ������������ "+ (fm1 - fm2));
        System.out.println("����� ������ �� ����������� ������������ "+ (tm1 -tm2));
    }
    
}
