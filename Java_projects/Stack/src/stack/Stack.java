package stack;

import java.util.Scanner;
/**
 *
 * @author Sh0N
 */

public class Stack {
    
    static class Deque {
        int size1,size2;       //������ 1-�� � 2-�� ����� �������������
        int size;              //������ �������
        int head;              //��������� 2-�� �����
        int tail;              //��������� 1-�� �����
        double[] data;         //������ ������
        
        Deque(int size1,int size2) {     //����������������� �������
            this.size1 = size1;
            this.size2 = size2;
            data = new double[this.size = size1 + size2];
            this.head = size - 1;
        }
        long PushBack(double value) {    //���������� 1-�� ����� ������ ����
            if (tail == size1) {
                System.out.println("���� ����������");
                //tail = 0;
                return 0;
            }
            long l1,l2;
            l1 = System.nanoTime();
            data[tail++] = value;
            l2 = System.nanoTime()-l1;
            return (l2);
        }
        long PopBack() {               //�������� ���������� ����������� �������� 1-�� �����
            long j1,j2;
            j1 = System.nanoTime();
            if (--tail < 0)
                tail = size1 - 1;
            data[tail] = 0;
            j2 = System.nanoTime() - j1;
            return (j2);
        }
        int PushFront(double value) {   //���������� 2-�� ����� ����� �����
            if (head < size1) {
                System.out.println("���� ����������");
                //head = size - 1;
                return -1;
            }
            data[head--] = value;
            return 0;
        }
        void PopFront() {              //�������� ���������� ����������� �������� 2-�� �����
            if (head == size) 
                head = size1;
            data[++head] = 0;
        }
        void Print(int s) {
            if (s != 0) {
                for (int i = 0; i < s; i++)
                    System.out.print(this.data[i] + " ");
                System.out.println();
            }
            if (s == 0)
                System.out.println("�������� ����!");
        }
        boolean isEmpty1() {             //�������� �� ������� 1-�� �����
            return (head == size - 1);
        }
        boolean isEmpty2() {             //�������� �� ������� 2-�� �����
            return (tail == 0);
        }
    }
    
    static class Item {
        Item next;              // ��������� �� ��������� �������
        double data;            // ������
    }
    
    static class FreeItem {
        FreeItem next;              // ��������� �� ��������� �������
        String data;            // ������
    }
    
    static class List {
        private Item head;       // ��������� �� ������ �������
        private Item tail;       // ��������� �� ��������� �������
        private FreeItem h;
        private FreeItem e;
        
        void addFree(String data) {         //�������� � ������ ������������� ���������
            FreeItem b = new FreeItem();
            b.data = data;
            
            if (h == null)
            {
                h = b;
                e = b;
            }
            else{
                b.next = h;
                h = b;
            }
        }
        
        long addFront(double data)         //�������� �������
        {
            long t1,t2;
            t1 = System.nanoTime();
            Item a = new Item();        
            a.data = data;              
                                    
            if(head == null)            
            {                          
                head = a;               
                tail = a;
            }
            else {
                a.next = head;          //����� ������� ��������� �� "������" ������
                head = a;               //��������� �� ������ ������� ��������� �� ����� ������� 
            }
            t2 = System.nanoTime() - t1;
            return(t2);
        }
        
        void addBack(double data) {          //���������� � ����� ������
            Item a = new Item();          
            a.data = data;
            if (tail == null)           
            {                           
                head = a;               
                tail = a;
            } 
            else {
                tail.next = a;          //"������" ��������� ������� ��������� �� �����
                tail = a;               //� ��������� �� ��������� ������� ���������� ����� ������ ��������
            }
        }
 
        void printList()                //������ ������
        {
            Item t = head;
            if (t == null)
                System.out.print("�������� ����!");
            else
                System.out.println("���� �������:");
            while (t != null)           
            {
                String h;
                h = t.toString();
                h = h.substring(17,25);
                System.out.print("("+h+")"+" "+"<>"+ " " + t.data + " ; "); 
                t = t.next;                     
            }
            System.out.println();
            FreeItem g = h;
            if (g == null)
                System.out.println("������ �� �������������!");
            else
                System.out.println("������ ������:");
            while (g != null)           
            {
                System.out.println(g.data);
                g = g.next;
            }
        }
 
        long delEl()          //�������� �������� �� ��������
        {
            long k1,k2;
            k1 = System.nanoTime(); 
            if(head == null){         //���� ����
                System.out.println("���� ����!");        
            }
            else {
                String g;
                g = head.toString();
                g = g.substring(17,25);
                addFree(g);
                head = head.next;
            }
            k2 = System.nanoTime() - k1; 
            return(k2);    
/* 
            if (head == tail) {     //���� ������� �� ������ ��������
                head = null;        
                tail = null;
                return;             
            }
 
            if (head.data == data) {    //���� ������ ������� - ���, ��� ��� �����
                head = head.next;       
                return;                 
            }
 
            Item t = head;                  
            while (t.next != null) {        
                if (t.next.data == data) {  
                    if(tail == t.next) {                       
                        tail = t;           
                    }
                    String h;
                    h = t.next.toString();
                    h = h.substring(17,25);
                    addFree(h);
                    t.next = t.next.next;   
                    return;                 
                }
                t = t.next;                
            }
            System.out.println("������ �������� ���!");
        */
        }
    }
    
    
    public static void main(String[] args) {
        Runtime u3 = Runtime.getRuntime();
        long fm1, fm2;
        long tm1, tm2;
        u3.gc();
        fm1 = u3.freeMemory();
        Deque mass = new Deque(0,0);
        fm2 = u3.freeMemory();
        tm1 = u3.freeMemory();
        List list = new List();
        tm2 = u3.freeMemory();
        long rt = 0;
        long tr = 0;
        long t2=0;
        long l2=0;
        long k2=0;
        long j2=0;
        //double t1,t2;
        //List flist = new List();
        int z = 0;
        String s1,s2,s3;
        Scanner sc = new Scanner(System.in);
        do {
            System.out.print("1 - ������ ������ 1-�� � 2-�� ����� �������; \n");
            System.out.print("2 - ��������� ���� ������; \n");
            System.out.print("3 - ��������� 1-�� ���� �������; \n");
            System.out.print("4 - ��������� 2-�� ���� �������; \n");
            System.out.print("5 - �������� ��������� ����� ������; \n");
            System.out.print("6 - �������� ��������� ����� �������; \n");
            System.out.print("7 - ������� ���� ������; \n");
            System.out.print("8 - ������� ���� �������; \n");
            System.out.print("9 - ��������� �������; \n");
            System.out.print("10 - �����; \n");
            s1 = sc.nextLine();
            z = Integer.parseInt(s1);
            switch (z){
                case 1:
                    int f,k;
                    System.out.print("������� ������ 1-�� �����: ");
                    s2 = sc.nextLine();
                    System.out.print("������� ������ 2-�� �����: ");
                    s3 = sc.nextLine();
                    try{
                        f = Integer.parseInt(s2);
                        k = Integer.parseInt(s3);
                        Runtime r = Runtime.getRuntime();
                        long mem1, mem2;
                        r.gc();
                        mem1 = r.freeMemory();
                        mass = new Deque(f,k);
                        mem2 = r.freeMemory();
                        rt = mem1-mem2;
                    }
                    catch(NumberFormatException nfe){
                        System.out.print("�������� ������ �����! \n");
                        System.exit(1);
                    }
                    break;
                case 2:
                    int j;
                    System.out.println("��������� ����� ���� �� ������� ������ <n>");
                    do {
                        System.out.println("1 - �������� �������");
                        System.out.println("2 - �������� � ����� (�� ������������ ������� �����)");
                        s2 = sc.nextLine();
                        if (!"n".equals(s2))
                        {
                            try{
                                double x = 0;
                                j = Integer.parseInt(s2);
                                System.out.println("������� ��������: ");
                                s3 = sc.nextLine();
                                try{
                                    x = Double.parseDouble(s3);
                                }
                                catch(NumberFormatException nfe){
                                    System.out.print("�������� ������ �����! \n");
                                    System.exit(1);
                                }
                                if (j == 1){
                                    Runtime r = Runtime.getRuntime();
                                    long mem1, mem2;
                                    r.gc();
                                    mem1 = r.freeMemory();
                                    t2 = list.addFront(x);
                                    mem2 = r.freeMemory();
                                    tr += mem1 - mem2;
                                }    
                                if (j == 2) {
                                    list.addBack(x);
                                }
                            }
                            catch(NumberFormatException nfe){
                                System.out.print("�������� ������ �����! \n");
                                System.exit(1);
                            }                          
                        }
                    } while (!"n".equals(s2));
                    break;
                case 3:
                    System.out.println("��������� 1-�� ����� ���� �� ������� ������ <n>");
                    do {
                        double bn;
                        System.out.print("������� ������� 1-�� �����: ");
                        s2 = sc.nextLine();
                        if (!"n".equals(s2))
                        {
                            try{
                                bn = Double.parseDouble(s2);
                                l2 = mass.PushBack(bn);                                
                                if (l2 == 0)
                                    s2 = "n";
                            }
                            catch(NumberFormatException nfe){
                                System.out.print("�������� ������ �����! \n");
                                System.exit(1);
                            }                          
                        }
                    } while (!"n".equals(s2));
                    break;
                case 4:
                    System.out.println("��������� 2-�� ����� ���� �� ������� ������ <n>");
                    do {
                        double bn = 0;
                        System.out.print("������� ������� 2-�� �����: ");
                        s2 = sc.nextLine();
                        if (!"n".equals(s2))
                        {
                            try{
                                int g = 0;
                                bn = Double.parseDouble(s2);
                                g = mass.PushFront(bn);
                                if (g == -1)
                                    s2 = "n";
                            }
                            catch(NumberFormatException nfe){
                                System.out.print("�������� ������ �����! \n");
                                System.exit(1);
                            }
                        }
                    } while (!"n".equals(s2));
                    break;
                case 5: 
                    double p;
                    //System.out.print("������� �������� �������� ����� ������: ");
                    //s2 = sc.nextLine();                    
                    k2 = list.delEl();
                    break;
                case 6:
                    int h = 0;
                    System.out.println("�������� � ����� ������� ���� �� ������� ������ <n>");
                    do {
                        System.out.println("1 - �������� ���������� �������� 1-�� �����");
                        System.out.println("2 - �������� ���������� �������� 2-�� �����");
                        s2 = sc.nextLine();
                        if (!"n".equals(s2))
                        {
                            try{
                                h = Integer.parseInt(s2);
                            }
                            catch(NumberFormatException nfe){
                                System.out.print("�������� ������ �����! \n");
                                System.exit(1);
                            }
                            if (h == 1)
                                if (mass.isEmpty2() == true) {
                                    System.out.println("���� ����");
                                    s2 = "n";
                                }
                                else {
                                    j2 = mass.PopBack();
                                }
                            if (h == 2)
                                if (mass.isEmpty1() == true) {
                                    System.out.println("���� ����");
                                    s2 = "n";
                                }
                                else
                                    mass.PopFront();
                            
                        }
                    } while (!"n".equals(s2));
                    break;
                case 7:
                    list.printList();
                    //flist.printList();
                    break;
                case 8:
                    if ((mass.isEmpty1() == true) && (mass.isEmpty2() == true))
                        System.out.println("���� ����");
                    else
                        mass.Print(mass.size);
                    break;
                case 9:
                    if ((l2 != 0) || (t2 != 0) || (j2 != 0) || (k2 != 0))
                    {
                        System.out.println("����� ���������� � ���� �������: "+l2+" ns");
                        System.out.println("����� ���������� � ���� ������: "+t2+" ns");
                        System.out.println("����� �������� � ���� �������: "+j2+" ns");
                        System.out.println("����� �������� � ���� ������: "+k2+" ns");
                        System.out.println("Memory used stack array: " + (rt - fm1 + fm2));
                        System.out.println("Memory used stack list: " + (tr - fm1 + fm2));
                    }
                    break;
            }
        } while (z != 10);
    }
    
}
