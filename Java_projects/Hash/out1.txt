digraph TG {  
node[shape=record, style="filled"]; 
"OY"[label="{OY|{<left>|<right>}}", fillcolor = white]; 
"OY": left -> "4Y";
"OY": right -> "b";
"4Y"[label="{4Y|{<left>|<right>}}", fillcolor = white]; 
"4Y": left -> "27";
"4Y": right -> "8n";
"27"[label="{27|{<left>|<right>}}", fillcolor = white]; 
"27": left -> "25";
"27": right -> "30";
"25"[label="{25|{<left>|<right>}}", fillcolor = white]; 
"30"[label="{30|{<left>|<right>}}", fillcolor = white]; 
"30": right -> "3B";
"3B"[label="{3B|{<left>|<right>}}", fillcolor = white]; 
"8n"[label="{8n|{<left>|<right>}}", fillcolor = white]; 
"8n": left -> "5W";
"8n": right -> "J1";
"5W"[label="{5W|{<left>|<right>}}", fillcolor = white]; 
"5W": right -> "7w";
"7w"[label="{7w|{<left>|<right>}}", fillcolor = white]; 
"J1"[label="{J1|{<left>|<right>}}", fillcolor = white]; 
"J1": right -> "Jr";
"Jr"[label="{Jr|{<left>|<right>}}", fillcolor = white]; 
"b"[label="{b|{<left>|<right>}}", fillcolor = white]; 
"b": left -> "a";
"b": right -> "nG";
"a"[label="{a|{<left>|<right>}}", fillcolor = white]; 
"a": left -> "Xs";
"a": right -> "ab";
"Xs"[label="{Xs|{<left>|<right>}}", fillcolor = white]; 
"Xs": right -> "Yw";
"Yw"[label="{Yw|{<left>|<right>}}", fillcolor = white]; 
"ab"[label="{ab|{<left>|<right>}}", fillcolor = white]; 
"ab": right -> "abc";
"abc"[label="{abc|{<left>|<right>}}", fillcolor = white]; 
"nG"[label="{nG|{<left>|<right>}}", fillcolor = white]; 
"nG": left -> "d";
"nG": right -> "qu";
"d"[label="{d|{<left>|<right>}}", fillcolor = white]; 
"d": right -> "f";
"f"[label="{f|{<left>|<right>}}", fillcolor = white]; 
"qu"[label="{qu|{<left>|<right>}}", fillcolor = white]; 
"qu": right -> "tG";
"tG"[label="{tG|{<left>|<right>}}", fillcolor = white]; 
} 