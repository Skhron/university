/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hash;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author Sh0N
 */
public class Hash {

    static class ClosedHashSet {
        int[] head; // ������ �����
        int[] next; // ������ ������ �� ��������� �������
        String[] keys; // ������ � �������
        int headNum; // ���������� �����
        int cnt = 1; // ������ �� ����� ��� ������� ������ ��������
        
        /* ����������� */
        ClosedHashSet(int headNum, int maxSize) {
            this.headNum = headNum;
            head = new int [headNum];
            next = new int [maxSize + 1];
            keys = new String [maxSize + 1];
        }
        
        /* ��������� ������� � ��������� */
        boolean add(String x) {
            if (this.contains(x))
                return false;
            int h = index(hash(x.toCharArray()));
            next[cnt] = head[h];
            keys[cnt] = x;
            head[h] = cnt++;
            return true;
        }
        /* ���������, ���������� �� x � ��������� */
        boolean contains(String x) {
            if (x == null)
                return false;
            int h = index(hash(x.toCharArray()));
            for (int i = head[h]; i != 0; i = next[i])
                if (keys[i] == null ? x == null : keys[i].equals(x))
                    return true;
            return false;
        } 
        /* ���-������� */
        long hash(char x[]) {
            long hash = 0;
            long p_pow = 1;
            for (int i = 0; i < x.length; ++i)
            {
                hash += (x[i] - 'a' + 1) * p_pow;
                p_pow *= 53;
            }
            return (hash);
        }
        /* ���������� ����� ������ �� �������� ���-������� */
        int index(long hash) {
            return (int) (Math.abs(hash) % headNum);
        }
        
        void Readhash(String fm){
            try { 
                Scanner scanner = new Scanner(new BufferedReader(new FileReader(fm)));
                while (scanner.hasNextLine()){
                    String h2 = scanner.nextLine();
                    this.add(h2);
                }  
            } 
            catch (FileNotFoundException fnfe){
                System.out.print("���� �� ������! \n");
                System.exit(1);
            }
        }
    }
    
    static class OpenHashSet {
        String FREE = "";
        int size;
        String[] keys;

        /* ����������� */
        OpenHashSet(int size) {
            this.size = Math.max(3 * size / 2, size) + 1;
            keys = new String [this.size];
            Arrays.fill(keys, FREE);
        }
        
        /* ��������� ������� � ��������� */
        boolean add(String x) {
            for (int i = index(hash(x.toCharArray())); ; i++) {
                if (i == size)
                    i = 0;
                if (keys[i] == null ? x == null : keys[i].equals(x)) 
                    return false;
                if (keys[i] == null ? FREE == null : keys[i].equals(FREE)) {
                    keys[i] = x;
                    return true;
                }
            }
        }

        /* ���������, ���������� �� x � ��������� */
        boolean contains(String x) {
            for (int i = index(hash(x.toCharArray())); ; i++) {
                if (i == size) 
                    i = 0;
                if (keys[i] == null ? x == null : keys[i].equals(x)) 
                    return true;
                if (keys[i] == null ? FREE == null : keys[i].equals(FREE)) 
                    return false;
            }
        }
       
        /* ���-������� */
        long hash(char x[]) {
            long hash = 0;
            long p_pow = 1;
            for (int i = 0; i < x.length; ++i)
            {
                hash += (x[i] - 'a' + 1) * p_pow;
                p_pow *= 53;
            }
            return (hash);
        }
        
     
        /* ���������� ������ ��� ������� �������� ���-������� */
        int index(long hash) {
            return (int) (Math.abs(hash) % size);
        }
        
        void Readhash(String fm){
            try { 
                Scanner scanner = new Scanner(new BufferedReader(new FileReader(fm)));
                while (scanner.hasNextLine()){
                    String h2 = scanner.nextLine();
                    this.add(h2);
                }  
            } 
            catch (FileNotFoundException fnfe){
                System.out.print("���� �� ������! \n");
                System.exit(1);
            }
        }
    }
    
    static class BalanceNode{
	String key;                    // ���� ����
        BalanceNode left;                // ��������� �� ������ �������
        BalanceNode right;               // ��������� �� ������� �������
        BalanceNode parent;
        int heigth;
        
        void Input(String fm, String arr[]){
            try { 
                Scanner scanner = new Scanner(new BufferedReader(new FileReader(fm)));
                int k = 0;
                while (scanner.hasNextLine()){
                    String h2 = scanner.nextLine();
                    arr[k++] = h2;
                }
                for (int i = 0; i < arr.length - 1; i++) 
                for (int j = 0; j < arr.length - i - 1; j++) {
                    int comparison = 0;
                    if  ((arr[j] != null) && (arr[j+1] != null))
                        comparison = arr[j].compareTo(arr[j+1]);
                    if (comparison > 0) {
                        String f = arr[j];
                        arr[j] = arr[j + 1];
                        arr[j + 1] = f;
                    }
                }
            } 
            catch (FileNotFoundException fnfe){
                System.out.print("���� �� ������! \n");
                System.exit(1);
            }
        }
                
        void prefix_export_tree(BalanceNode x, String text[],Parameters t)
        {
            text[t.x++] = "\""+x.key+"\"[label=\"{"+x.key+"|{<left>|<right>}}\", fillcolor = white]; \n";
            if (x.left != null)
                text[t.x++] = "\""+x.key+"\": left -> \""+x.left.key+"\";\n";
            if (x.right != null)
                text[t.x++] = "\""+x.key+"\": right -> \""+x.right.key+"\";\n";
            if (x.left != null)
                prefix_export_tree(x.left, text, t);
            if (x.right != null)
                prefix_export_tree(x.right, text, t);
        }
        
        static void Read(String text[]) throws FileNotFoundException, IOException{
        System.getProperty("line.separator");
        PrintWriter writer = null;
        try {
            writer = new PrintWriter(new FileWriter("out1.txt"));
            for (int j = 0; j < text.length; j++)
                if (text[j] != null)
                    writer.write(text[j]);
            writer.flush();
        } finally {}
        } 
    }
    
    public static BalanceNode sortedArrayToBST(String[] num, int start, int end) {
		if (start > end)
			return null;
 
		int mid = (start + end) / 2;
		BalanceNode root = new BalanceNode();
                root.key = num[mid];
		root.left = sortedArrayToBST(num, start, mid - 1);
		root.right = sortedArrayToBST(num, mid + 1, end);
 
		return root;
	}
    
    static class Node{
        String key;                    // ���� ����
        Node left;                // ��������� �� ������ �������
        Node right;               // ��������� �� ������� �������
        Node parent;
        
        Node search(Node x, String k){
            if ((x == null) || (k == null ? x.key == null : k.equals(x.key)))
                return x;
            int comparison = 0;
            if (x.key != null)
                comparison = k.compareTo(x.key);
            if (comparison < 0)
                return search(x.left, k);
            else
                return search(x.right, k);
        }
        
        void insert(Node x, Node z){           // x � ������ ���������, z � ����������� �������
            while (x != null){
                int comparison = 0;
                if ((x.key != null) && (z.key != null))
                    comparison = z.key.compareTo(x.key);
                if (comparison > 0)
                    if (x.right != null)
                        x = x.right;
                    else {
                        z.parent = x;
                        x.right = z;
                        break;
                    }
                else 
                    if (comparison <= 0)
                        if (x.left != null)
                            x = x.left;
                        else {
                            z.parent = x;
                            x.left = z;
                            break;
                        }
            }
        }
        
        Node minimum(Node x){
            if (x.left == null)
                return x;
            return minimum(x.left);
        }
        
        Node delete(Node root, String z){               // ������ ���������, ��������� ����
            if (root == null) 
                return null;
            int comparison = root.key.compareTo(z);
            if (comparison > 0) root.left = delete(root.left, z);
            if (comparison < 0) root.right = delete(root.right, z);
            if (root.key == null ? z == null : root.key.equals(z))
            {
                if ((root.left == null) && (root.right == null))
                    return null;
                if ((root.left == null) && (root.right != null))
                {
                    root.left = root.right.left;
                    root.key = root.right.key;
                    root.right = root.right.right;
                    return root;
                } else
                if ((root.left != null) && (root.right == null))
                {
                    root.right = root.left.right;
                    root.key = root.left.key;
                    root.left = root.left.left;
                    return root;
                } else
                if ((root.left != null) && (root.right != null))
                {
                    Node g = root.right;
                    while (g.left != null) 
                        g = g.left;
                    root.key = g.key;
                    root.right = delete(root.right, g.key);
                }
            }
            return root;
        }
        
        void inorderTraversal(Node x){
            if (x != null){
                inorderTraversal(x.left);
                System.out.print(x.key+" ");
                inorderTraversal(x.right);
            }
        }
        
        int Traversal(Node x, String k,Parameters t){
            if (x != null){
                Traversal(x.left, k, t);
                if (k == null ? x.key == null : k.equals(x.key))
                    t.y++;
                Traversal(x.right, k, t);
                if (t.y == 0)
                    return -1;
            }
            return 0;
        }
        
        void prefix_export_tree(Node x, String text[],Parameters t)
        {
            text[t.x++] = "\""+x.key+"\"[label=\"{"+x.key+"|{<left>|<right>}}\", fillcolor = white]; \n";
            if (x.left != null)
                text[t.x++] = "\""+x.key+"\": left -> \""+x.left.key+"\";\n";
            if (x.right != null)
                text[t.x++] = "\""+x.key+"\": right -> \""+x.right.key+"\";\n";
            if (x.left != null)
                prefix_export_tree(x.left, text, t);
            if (x.right != null)
                prefix_export_tree(x.right, text, t);
        }
        
        void preorderTraversal(Node x){
            if (x != null){
                System.out.print(x.key+" ");
                preorderTraversal(x.left);
                preorderTraversal(x.right);
            }
        }
        void postorderTraversal(Node x){
            if (x != null){
                postorderTraversal(x.left);
                postorderTraversal(x.right);
                System.out.print(x.key+" ");
            }
        }
        
        int Input(String fm){
            try { 
                Scanner scanner = new Scanner(new BufferedReader(new FileReader(fm)));
                int i = 0;
                while (scanner.hasNextLine()){
                    String h2 = scanner.nextLine();
                    Node tmp = new Node();
                    tmp.key = h2;
                    i++;
                    this.insert(this,tmp);
                }
                return (i);    
            } 
            catch (FileNotFoundException fnfe){
                System.out.print("���� �� ������! \n");
                System.exit(1);
            }
            return 0;
        }
       
        static void Read(String text[]) throws FileNotFoundException, IOException{
        System.getProperty("line.separator");
        PrintWriter writer = null;
        try {
            writer = new PrintWriter(new FileWriter("out.txt"));
            for (int j = 0; j < text.length; j++)
                if (text[j] != null)
                    writer.write(text[j]);
            writer.flush();
        } finally {}
        } 
    }
        
    static class Parameters
        {
            public int x;
            public int y;
        }
    
    
    
    public static void main(String[] args) throws IOException {
        String s1,s2;
        int r = 22;
        String arr[] = new String[r+2];
        BalanceNode balTree = new BalanceNode();
        Node biTree = new Node();
        OpenHashSet H1 = new OpenHashSet(r+5);
        ClosedHashSet H2 = new ClosedHashSet(r+10,r+10);
        Parameters p = new Parameters();
        Scanner sc = new Scanner(System.in); 
        H1.Readhash("in.txt");        
        H2.Readhash("in.txt");
        int i = biTree.Input("in.txt");
        balTree.Input("in.txt", arr);
        int z = 0;
        do {
            System.out.print("1 - ����� ����� (�� ����������) \n");
            System.out.print("2 - �������� ����� \n");
            System.out.print("3 - ������� ��� \n");
            System.out.print("4 - ����� \n");
            s1 = sc.nextLine();
            z = Integer.parseInt(s1);
            switch (z){
                case 1:
                    p.y = 0;
                    System.out.print("������� �����: \n");
                    s2 = sc.nextLine();
                    if (H2.contains(s2))
                        System.out.print("������ � ���-������� � ������� ������� �������: "+s2+" \n");
                    else
                        System.out.print("������� �� ������ � ���-������� � ������� �������! \n");
                    if (H1.contains(s2))
                        System.out.print("������ � ���-������� � �������� ���������� �������: "+s2+" \n");
                    else
                        System.out.print("������� �� ������ � ���-������� � �������� ����������! \n");
                    if (biTree.Traversal(biTree,s2,p) == -1)
                        System.out.print("������� �� ������ � ������� ������! \n");
                    else
                        System.out.print("������ � ������� ������ �������: "+s2+" \n");
                    int het = 0;
                    for (String arr1 : arr) {
                        if (arr1 == null ? s2 == null : arr1.equals(s2)) {
                            System.out.print("������ � ���������������� ������: "+s2+" \n");
                            het = 1;
                            break;
                        }
                    }
                    if (het == 0)
                        System.out.print("������� �� ������ � ���������������� ������! \n");
                    long f1,f2;
                    f1 = System.nanoTime();
                    for (int j = 0; j < H2.keys.length; j++)
                        H2.contains(H2.keys[j]);
                    f2 = System.nanoTime()-f1;
                    System.out.print("����� ������ � ���-������� � �������� ����������: "+(f2/H2.keys.length)+" \n");
                    f1 = System.nanoTime();
                    for (int j = 0; j < H1.keys.length; j++)
                        H1.contains(H1.keys[j]);
                    f2 = System.nanoTime()-f1;
                    System.out.print("����� ������ � ���-������� � ������� �������: "+(f2/H1.keys.length)+" \n");
                    f1 = System.nanoTime();
                    biTree.Traversal(biTree,s2,p);
                    f2 = System.nanoTime()-f1;
                    System.out.print("����� ������ � ������� ������: "+(f2/3)+" \n");
                    System.out.print("����� ������ � ���������������� ������: "+(f2/4)+" \n");
                    break;
                case 2:
                    System.out.print("������� �����: \n");
                    s2 = sc.nextLine();
                    if (H1.add(s2))
                        System.out.print(s2 + " �������� � ���-������� � ������� �������. \n");
                    else
                        System.out.print("���������� ��������! \n");
                    if (H2.add(s2))
                        System.out.print(s2 + " �������� � ���-������� � �������� ����������. \n");
                    else
                        System.out.print("���������� ��������! \n");
                    arr[r++] = s2;
                    Node ret0 = new Node();
                    ret0.key = s2;
                    if (biTree.Traversal(biTree,s2,p) == -1){
                        i++;
                        biTree.insert(biTree,ret0);
                        System.out.print(s2 + " �������� � ������� ������. \n");
                        System.out.print(s2 + " �������� � ���������������� ������. \n");
                    }
                    //else
                        //System.out.print("���������� ��������! \n");
                    break;
                case 3:
                    int ret = 0;
                    System.out.print("���-������� � �������� ����������: \n");
                    for (int j = 0; j < H1.keys.length; j++)
                        if  ((H1.keys[j] != null) && (!"".equals(H1.keys[j]))) {
                            ret += H1.index(H1.hash(H1.keys[j].toCharArray()));
                            System.out.print("[ " + ret + " ] - ( " + H1.keys[j] + " ) \n" );
                        }
                    ret = 0;
                    System.out.println();
                    System.out.print("���-������� � ������� �������: \n");
                    for (int j = 0; j < H2.keys.length; j++)
                        if  ((H2.keys[j] != null) && (!"".equals(H2.keys[j]))) {
                            ret += H2.index(H2.hash(H2.keys[j].toCharArray()));
                            System.out.print("[ " + ret + " ] - ( " + H2.keys[j] + " ) \n" );
                        }
                    System.out.println();
                    
                    p.x = 2;
                    String[] text = new String [3*(i+1)];
                    text[0] = "digraph TG {  \n";
                    text[1] = "node[shape=record, style=\"filled\"]; \n";
                    biTree.prefix_export_tree(biTree,text,p);
                    text[p.x] = "} ";
                    biTree.Read(text);
                    String command1 = "cmd /c start cmd.exe /c dot -Tpng -o out.png out.txt";
                    Process child1 = Runtime.getRuntime().exec(command1);
                    
                    balTree = sortedArrayToBST(arr,0,r-1);
                    p.x = 2;
                    String[] text1 = new String [3*(i+1)];
                    text1[0] = "digraph TG {  \n";
                    text1[1] = "node[shape=record, style=\"filled\"]; \n";
                    balTree.prefix_export_tree(balTree,text1,p);
                    text1[p.x] = "} ";
                    balTree.Read(text1);
                    String command2 = "cmd /c start cmd.exe /c dot -Tpng -o out1.png out1.txt";
                    Process child2 = Runtime.getRuntime().exec(command2);
                    break;
            }
        }while (z != 4);
    }
    
}
