/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aa_zachet;

import java.util.Random;

/**
 *
 * @author shaun
 */
public class AA_Zachet {
    
    public static int[][] multiply(int[][] a, int[][] b) {
 
        int rowsA = a.length;
        int columnsB = b[0].length;
        int columnsA_rowsB = a[0].length;
 
        int[][] c = new int[rowsA][columnsB];
 
        for (int i = 0; i < rowsA; i++) {
            for (int j = 0; j < columnsB; j++) {
                int sum = 0;
                for (int k = 0; k < columnsA_rowsB; k++) {
                    sum += a[i][k] * b[k][j];
                }
                c[i][j] = sum;
            }
        }
 
        return c;
    }
    
    private static int[][] summation(int[][] a, int[][] b) {
 
        int n = a.length;
        int m = a[0].length;
        int[][] c = new int[n][m];
 
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                c[i][j] = a[i][j] + b[i][j];
            }
        }
        return c;
    }
    
    private static int[][] subtraction(int[][] a, int[][] b) {
 
        int n = a.length;
        int m = a[0].length;
        int[][] c = new int[n][m];
 
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                c[i][j] = a[i][j] - b[i][j];
            }
        }
        return c;
    }
    
    private static void splitMatrix(int[][] a, int[][] a11, int[][] a12, int[][] a21, int[][] a22) {
        int n = a.length >> 1;

        for (int i = 0; i < n; i++) {
            System.arraycopy(a[i], 0, a11[i], 0, n);
            System.arraycopy(a[i], n, a12[i], 0, n);
            System.arraycopy(a[i + n], 0, a21[i], 0, n);
            System.arraycopy(a[i + n], n, a22[i], 0, n);
        }
    }
    
    private static int[][] collectMatrix(int[][] a11, int[][] a12, int[][] a21, int[][] a22) {
        int n = a11.length;
        int[][] a = new int[n << 1][n << 1];

        for (int i = 0; i < n; i++) {
            System.arraycopy(a11[i], 0, a[i], 0, n);
            System.arraycopy(a12[i], 0, a[i], n, n);
            System.arraycopy(a21[i], 0, a[i + n], 0, n);
            System.arraycopy(a22[i], 0, a[i + n], n, n);
        }
        return a;
    }
    
    private static int[][] multiStrassen(int[][] a, int[][] b, int n) {
        if (n <= 1) {
            return multiply(a, b);
        }
        n = n >> 1;

        int[][] a11 = new int[n][n];
        int[][] a12 = new int[n][n];
        int[][] a21 = new int[n][n];
        int[][] a22 = new int[n][n];

        int[][] b11 = new int[n][n];
        int[][] b12 = new int[n][n];
        int[][] b21 = new int[n][n];
        int[][] b22 = new int[n][n];

        splitMatrix(a, a11, a12, a21, a22);
        splitMatrix(b, b11, b12, b21, b22);

        int[][] c11 = summation(multiStrassen(a11,b11,n), multiStrassen(a12,b21,n));
        int[][] c12 = summation(multiStrassen(a11,b12,n), multiStrassen(a12,b22,n));
        int[][] c21 = summation(multiStrassen(a21,b11,n), multiStrassen(a22,b21,n));
        int[][] c22 = summation(multiStrassen(a21,b12,n), multiStrassen(a22,b22,n));
        
        printMatrix(collectMatrix(c11, c12, c21, c22));

        return collectMatrix(c11, c12, c21, c22);
    }
    
    public static int[][] randomMatrix(int m, int n) {
        int[][] a = new int[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                a[i][j] = new Random().nextInt(10);
            }
        }
        return a;
    }
    
    public static void printMatrix(int[][] a) {
        for (int i = 0; i < a[0].length; i++) {
            System.out.print("-------");
        }
        System.out.println();
        for (int[] anA : a) {
            System.out.print("|");
            for (int anAnA : anA) {
                System.out.printf("%4d |", anAnA);
            }
 
            System.out.println();
            for (int i = 0; i < a[0].length; i++) {
                System.out.print("-------");
            }
            System.out.println();
        }
    }
    
    public static void main(String[] args) {
        // TODO code application logic here
        int A[][] = randomMatrix(4,4);
        int B[][] = randomMatrix(4,4);
        int C[][] = multiStrassen(A,B,4);
        printMatrix(A);
        System.out.println("*");
        printMatrix(B);
        System.out.println("=");
        printMatrix(C);
    }
    
}
