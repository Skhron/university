package tisd;
import java.util.Scanner;
import java.math.*;
public class TiSD {
    // Функция суммы длинных целых
    public static String Addition(char[] a){
        int t = 0;
        String s = "";
        int length = a.length+1;
        int[] x = new int[length];
        for (int i = a.length-1; i >= 0; i--)  
            x[a.length-1-i] = a[i] - 48;
        x[0] += 1;
        for (int i = 0; i < length-1; i++){
            x[i+1] += (x[i]/10);
            x[i] %= 10;
        }
        if (x[length - 1] == 0)
            length--;
        for(int i = length-1; i >= 0; i--){
            if ((x[i] != 0) || (t != 0)){
                t = 1;
                s += String.valueOf(x[i]);
            }
        }
        return (s);
    }
    // Функция умножения длинных целых
    public static String Multiplication(char[] a, char[] b){
        int[] x = new int[a.length];
        int[] y = new int[b.length];
        int[] c = new int[a.length+b.length+1];
        String s = "";
        int temp; 
        // Перевод из массива типа char в массив типа integer
        for (int i = a.length-1; i >= 0; i--){
            if (Character.isDigit(a[i]) == false){
                System.out.print("Неправильный формат ввода! \n");
                System.exit(1);
            }    
            x[a.length-1-i] = a[i] - 48;
        }
        for (int i = b.length-1; i >= 0; i--){
            if (Character.isDigit(b[i]) == false){
                System.out.print("Неправильный формат ввода! \n");
                System.exit(1);
            }
            y[b.length-1-i] =  b[i] -48;
        }
        // Умножение столбиком
        for (int i = 0; i < a.length; i++){
            int p = 0; // переменная переноса
            for (int j = 0; j < b.length; j++){
                temp = x[i]*y[j]+ p +c[i+j];
                c[i+j] = temp%10;
                p = temp/10;
            }
            c[i+b.length] = p;
        }
        int t = 0;
        // Переворот 
        for(int i = c.length-1; i >= 0; i--){
            if ((c[i] != 0) || (t != 0)){
                t = 1;
                s += String.valueOf(c[i]);
            }
        }
        if ("".equals(s))
            s += '0';
        return(s);
    }

    public static void main(String[] args) {
        int temp1 = 0,temp2 = 0; // позиция '.' и 'E' соответственно
        int rc = 0;              // код ошибки
        char c = '0';            // знак
        int mantis = 0;          // порядок
        // Ввод чисел
        Scanner sc = new Scanner(System.in);
        System.out.print("Программа умножает целое(в формате +-h) число на действительное (в формате +-m.nE+-k) \n");
        System.out.print("Введите целое число: ");
        String s1, s2, cash1, cash2;
        s1 = sc.nextLine();
        System.out.print("                                                              | \n");
        System.out.print("Введите действительное число: ");
        s2 = sc.nextLine();
        // Нахождение позиций
        temp2 = s2.indexOf('E');
	temp1 = s2.indexOf('.');
        if ((temp1 == -1) || (temp1 > 31) || (temp1 != s2.lastIndexOf('.')))
            rc = 1;
        if ((temp2 == -1) || (temp2 > 32) || (temp2 != s2.lastIndexOf('E')))
            rc = 1;
        // Работа со знаками
        if ((s2.indexOf('-') == 0) && (s1.indexOf('-') == 0))
            c = '+';
        if ((s2.indexOf('-') == 0) && (s1.indexOf('+') == 0))
            c = '-';
        if ((s2.indexOf('+') == 0) && (s1.indexOf('+') == 0))
            c = '+';
        if ((s2.indexOf('+') == 0) && (s1.indexOf('-') == 0))
            c = '-';
        // Обработка неправильного ввода
        if ((c == '0') || ((s2.lastIndexOf('-') != temp2+1) && (s2.lastIndexOf('+') != temp2+1))) 
            rc = 1;
        if (s1.length() > 31)
            rc = 1;
        if (rc == 1){
            System.out.print("Неверный формат ввода! \n");
            System.exit(rc);
        }
        // Преобразование и разбиение чисел
        try{
            mantis = Integer.parseInt(s2.substring(temp2+1,s2.length()));
        }
        catch(NumberFormatException nfe){
            System.out.print("Неверный формат ввода! \n");
            System.exit(1);
        }
        cash1 = s2.substring(1,temp1) + s2.substring(temp1+1,temp2);
        cash2 = s1.substring(1,s1.length());
        
        while ((cash1.lastIndexOf('0') == cash1.length()-1) && (cash1.length() != 1)) 
            cash1 = cash1.substring(0,cash1.length()-1);
        while ((cash1.indexOf('0') == 0) && (cash1.length() != 1)){
            cash1 = cash1.substring(1,cash1.length());
            mantis--;
        }
        while ((cash2.lastIndexOf('0') == cash2.length()-1) && (cash2.length() != 1)){
            cash2 = cash2.substring(0,cash2.length()-1);
            mantis++;
        }

        
        String end = Multiplication(cash1.toCharArray(),cash2.toCharArray());
        String temp3 = s2.substring(1,temp1);
        mantis += end.length()-(cash1.length()-temp3.length());
        if (end.length() > 30)
            if (end.charAt(30) >= '5'){
                end = end.substring(0,30);
                end = Addition(end.toCharArray());
            }
            else
                end = end.substring(0,30);
        // Обработка результат умножения
        while ((end.lastIndexOf('0') == end.length()-1) && (end.length() != 1))
            end = end.substring(0,end.length()-1);
        if ("0".equals(end))
            mantis = 0;
        if (Math.abs(mantis) > 99999){
            System.out.print("Переполнение! \n");
            rc = 2;
            System.exit(rc);
        }
        //Вывод
        System.out.print("Результат: ");
        if (mantis < 0)
            System.out.print(c+"0."+end+'E'+mantis+"\n");
        else
            System.out.print(c+"0."+end+'E'+'+'+mantis+"\n");
        BigDecimal d = new BigDecimal(s1);
        BigDecimal e = new BigDecimal(s2);
        BigDecimal f = d.multiply(e);
        System.out.print("Результат:    "+f.toString()+"\n");
    }
    
}
